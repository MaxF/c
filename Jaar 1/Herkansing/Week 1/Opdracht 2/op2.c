#include <stdio.h>

#define SIZE 6

void printSwapPrint(int *numbers);

int main()
{
	int intNumbers[SIZE] = { 5, 23, 532, 6, 834, 93 };
	printSwapPrint(&intNumbers[0]);
	return 0;
}

void printSwapPrint(int *numbers)
{
	int i, reverse[SIZE];
	for(i = 0; i < SIZE; i++)
	{
		printf("%d, ", numbers[i]);
		reverse[i] = numbers[(SIZE - i) - 1];
	}
	printf("\n");
	for(i = 0; i < SIZE; i++)
	{
		printf("%d, ", reverse[i]);
	}
	printf("\n");
}
