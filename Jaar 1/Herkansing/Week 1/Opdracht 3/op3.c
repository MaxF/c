#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 20

int findInt(int *numbers, int size, int wanted);
void fillRandomArray(int *array, int size, int range);

int main()
{
	int numbers[SIZE], index_position, i, wanted_number;
	while(1)
	{
		printf("Voer een getal tussen de 0 en 49 in: ");
		scanf("%d", &wanted_number);
		fillRandomArray(&numbers[0], SIZE, 50);
		index_position = findInt(&numbers[0], SIZE, wanted_number);
		if(index_position != -1)
		{
			printf("Het ingevoerde getal (%d), is het %de element in de array van %d elementen met waarden tussen de 0 en 49\n", wanted_number, index_position, SIZE);
			printf("Check het zelf!\n");
			for(i = 0; i < SIZE; i++)
			{
				printf("Element %d = %d\n", i , numbers[i]);
			}
			break;
		}
		else
		{
			printf("Het ingevoerde getal komt niet in de random gegenereerde array voor, probeer het opnieuw!\n");
		}
	}
	return 0;
}

int findInt(int *numbers, int size, int wanted)
{
	int i;
	for(i = 0; i < size; i++)
	{
		if(numbers[i] == wanted)
		{
			return i;
		}
	}
	return -1;
}

void fillRandomArray(int *array, int size, int range)
{
	int i;
	srand(time(NULL));
	for(i = 0; i < size; i++)
	{
		array[i] = rand() % range;
	}
}
