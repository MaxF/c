#include <stdio.h>

#define Amount 6
#define Biggest 3

void printBiggestThree(int *getallen, int arrayLength);

int main()
{
	int numbers [Amount] = { 5, 7, 3, 24, 9, 98 };
	printBiggestThree(&numbers[0], (sizeof(numbers) / sizeof(int)));
	return 0;
}

void printBiggestThree(int *getallen, int arrayLength)
{
	int i, j, biggest[Biggest] = { 0, 0, 0 };
	if(arrayLength >= 3)
	{
		for(i = 0; i < Biggest; i++)
		{
			for(j = 0; j < Amount; j++)
			{
				if(biggest[i] < getallen[j])
				{
					biggest[i] = getallen[j];
				}
			}
		}
		printf("Van de getallen: ");
		for(i = 0; i < Amount; i++)
		{
			printf("%d, ", getallen[i]);
		}
		printf("zijn ");
		for(i = 0; i < Biggest; i++)
		{
			printf("%d, ", biggest[i]);
		}
		printf("de grootste!\n");
	}
	else
	{
		printf("De int-array moet minimaal 3 elementen bevatten!\n");
	}
}
