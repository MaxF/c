#include <stdio.h>

#define SIZE 7

void sortAlpha(char *chars);

int main()
{
	char characters[SIZE] = { 'd', 'a', 'c', 't', 'b', 'o', 'v' };
	sortAlpha(&characters[0]); 
	return 0;
}

void sortAlpha(char *chars)
{
	int i, j, a = 97, z = 122, teller = 0;
	char reversedChars[SIZE];
	printf("De originele volgorde is: %s\n", chars);
	for(i = a; i < z; i++)
	{
		for(j = 0; j < SIZE; j++)
		{
			if(i == chars[j])
			{ 
				reversedChars[teller] = i;
				teller++;
			}
		}
	}
	printf("De string omgedraaid is: %s\n", reversedChars);
}
