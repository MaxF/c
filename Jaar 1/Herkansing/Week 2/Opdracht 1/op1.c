#include <stdio.h>

void generateBinairyString(int number);

int main()
{
		int number;
		printf("Geef een getal: ");
		scanf("%d", &number);
		generateBinairyString(number);
		return 0;
}

void generateBinairyString(int number)
{
	char binairyString[50];
	int i = 0, j = 0, temp = number, length;
	while(temp != 0)
	{
		if(j == 4)
		{
			binairyString[i] = ' ';
			j = 0;
		}
		else
		{
			binairyString[i] = temp % 2;
			temp /= 2;
		}
		i++;
		j++;
	}
	length = i;
	printf("Het getal %d is binair: ", number);
	for(i = length; i != 0; i--)
	{
		if(binairyString[i - 1] == ' ') printf(" ");
		else printf("%d", binairyString[i - 1]);
	}
	printf("\n");
}
