#include <stdio.h>

typedef enum { false, true } bool;

int askNumber(char *text);
bool compareDigits(int number1, int number2);

int main()
{
	int number1, number2;
	char message[19] = "Voer een getal in: ";
	number1 = askNumber(&message[0]);
	number2 = askNumber(&message[0]);
	if(compareDigits(number1, number2)) printf("De getallen %d en %d zijn aan elkaar gelijk!\n", number1, number2);
	else printf("De getallen %d en %d zijn niet aan elkaar gelijk!\n", number1, number2);
	return 0;
}

int askNumber(char *text)
{
	int i, number;
	for(i = 0; text[i] != '\0'; i++)
	{
		printf("%c", text[i]);
	}
	scanf("%d", &number);
	return number;
}

bool compareDigits(int number1, int number2)
{
	if(number1 == number2) return true;
	else return false;
}
