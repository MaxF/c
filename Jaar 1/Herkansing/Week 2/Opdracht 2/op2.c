#include <stdio.h>

#define binairyLength 39

void generateBinairyString(int number);
void XOR_Numbers(int number1, int number2);

int main()
{
	int number1, number2;
	printf("Geef getal 1: ");
	scanf("%d", &number1);
	printf("Geef getal 2: ");
	scanf("%d", &number2);
	generateBinairyString(number1);
	generateBinairyString(number2);
	XOR_Numbers(number1, number2);
	return 0;
}

void generateBinairyString(int number)
{
	char binairyString[50];
	int i, j = 0, temp = number;
	for(i = 0; i < binairyLength; i++)
	{
		j++;
		if(j == 5)
		{
			binairyString[i] = ' ';
			j = 0;
		}
		else
		{
			binairyString[i] = temp % 2;
			temp /= 2;
		}
	}
	printf("Het getal %10d is binair: ", number);
	for(i = binairyLength; i != 0; i--)
	{
		if(binairyString[i - 1] == ' ') printf(" ");
		else printf("%d", binairyString[i - 1]);
	}
	printf("\n");
}

void XOR_Numbers(int number1, int number2)
{
	char binairyString[binairyLength];
	int i, j, XOR = number1 ^ number2, count = 0, temp = XOR;
	printf("XOR van beide getallen: \t");
	for(i = 0; i < binairyLength; i++)
	{
		j++;
		if(j == 5)
		{
			binairyString[i] = ' ';
			j = 0;
		}
		else
		{
			binairyString[i] = temp % 2;
			temp /= 2;
		}
	}
	for(i = binairyLength; i != 0; i--)
	{
		if(binairyString[i - 1] == ' ') printf(" ");
		else printf("%d", binairyString[i - 1]);
		if(binairyString[i - 1] == 0) count++;
	}
	printf("\nDe getallen %d en %d komen op %d bitposities overeen!\n", number1, number2, count);
}
