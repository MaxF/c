#include <stdio.h>

typedef struct opdrachtnemers {
		char *voornaam;
		char *achternaam;
		int leeftijd;
		double uurtarief;
		int uren;
} opdrachtnemer;

void rapporteerBeloningen(opdrachtnemer *opdrNemers, int size);

int main()
{
	opdrachtnemer opdrNemers[] = {
			{"Koos", "Korswagen", 32, 135.75, 89},
			{"Bolus", "Bankpok", 44, 162.25, 24},
			{"Bonkige", "Harry", 26, 89.45, 46}
	};
	int s = sizeof(opdrNemers)/sizeof(opdrachtnemer);

	rapporteerBeloningen(opdrNemers, s);
	return 0;
}

void rapporteerBeloningen(opdrachtnemer *opdrNemers, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		printf("%s %s \t(%d)\t heeft recht op %8.2f euro\n", opdrNemers[i].voornaam, opdrNemers[i].achternaam, opdrNemers[i].leeftijd, (opdrNemers[i].uren * opdrNemers[i].uurtarief));
	}
}
