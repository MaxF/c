#include <stdio.h>

#define MAX_STR_LEN 50

int checkLength(char *cstr);
void reverse(char *cstr);

int main()
{
	char a[MAX_STR_LEN];
	printf("Geef een string (max %d tekens):", MAX_STR_LEN);
	fgets(a, MAX_STR_LEN, stdin);
	reverse(a);
	printf("%s\n", a);
	return 0;
}

int checkLength(char *cstr)
{
	int size = 0;
	while(cstr[size] != '\0') size++;
	return size;
}

void reverse(char *cstr)
{
	int i, temp, length = checkLength(cstr);
	for(i = 0; i < (length / 2); i++)
	{
		temp = cstr[i];
		cstr[i] = cstr[length - i - 1];
		cstr[length - i - 1] = temp;
	}
}
