#include <stdio.h>

typedef enum { false, true } bool;

bool deel_geheel(unsigned int teller, unsigned int noemer, unsigned int *quotient, unsigned int *rest);

int main()
{
	unsigned int teller, noemer, quotient, rest;
	printf("Geef de teller: ");
	scanf("%d", &teller);
	printf("Geef de noemer: ");
	scanf("%d", &noemer);
	if(deel_geheel(teller, noemer, &quotient, &rest)) printf("%d / %d = %d rest %d\n", teller, noemer, quotient, rest);
	else printf("AUB niet delen door 0\n");
	return 0;
}

bool deel_geheel(unsigned int teller, unsigned int noemer, unsigned int *quotient, unsigned int *rest)
{
	if(noemer == 0) return false;
	*quotient = teller / noemer;
	*rest = teller % noemer;
	return true;
}
