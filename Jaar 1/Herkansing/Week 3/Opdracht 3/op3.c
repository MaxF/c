#include <stdio.h>

typedef enum { false, true } bool;
#define SIZE 50

int checkLength(char *cstr);
bool is_palindrome(char *cstr);

int main()
{
	char input[SIZE];
	printf("Voer een woord in: ");
	scanf("%s", input);
	if(is_palindrome(input)) printf("Het ingevoerde woord is een palindroom!\n");
	else printf("Het ingevoerde woord is geen palindroom\n");
	return 0;
}

int checkLength(char *cstr)
{
	int size = 0;
	while(cstr[size] != '\0') size++;
	return size;
}

bool is_palindrome(char *cstr)
{
	bool state = true;
	int i, size = checkLength(cstr), head = 0, tail = size - 1;
	for(i = 0; i < (size / 2); i++)
	{
		if(cstr[head] != cstr[tail])
		{
			state = false;
			break;
		}
		head++;
		tail--;
	}
	return state;
}
