#include <stdio.h>
#include <math.h>

#define MIN_X -39
#define MIN_Y -12
#define MAX_X 40
#define MAX_Y 12

typedef struct {
		int x;
		int y;
} point;

typedef struct {
		point begin;
		point end;
} line;

void plot_grid(line *k);
//void plot_line(line *k);
int biggest(int a, int b);

int main()
{
	point begin = { -25, -7 };
	point end = { 30, 5 };
	line Line = { begin, end };
	plot_grid(&Line);
	return 0;
}

void plot_grid(line *k)
{
	int i, j, y;
	double a, b;

	if(biggest(k -> begin.x, k -> end.x) == k -> begin.x)
	{
		a = (double)(k -> begin.y - k -> end.y) / (double)(k -> begin.x - k -> end.x);
	}
	else
	{
		a = (double)(k -> end.y - k -> begin.y) / (double)(k -> end.x - k -> begin.x);
	}
	b = (double)-(a * k -> end.x) + (double)(k -> end.y);

	for(i = MAX_Y; i > MIN_Y; i--)
	{
		for(j = MIN_X; j < MAX_X; j++)
		{
			y = round((a * j) + b);
			if(i == y)
			{
				printf("*");
			}
			else if(i == 0 && j == 0)
			{
				printf("+");
			}
			else if(j == 0)
			{
				printf("|");
			}
			else if(i == 0)
			{
				printf("-");
			}
			else
			{
				printf(" ");
			}
		}
		printf("\n");
	}
}

/*void plot_line(line *k)
{
	double a, b;
	if(biggest(k -> begin.x, k -> end.x) == k -> begin.x)
	{
		a = (double)(k -> begin.y - k -> end.y) / (double)(k -> begin.x - k -> end.x);
	}
	else
	{
		a = (double)(k -> end.y - k -> begin.y) / (double)(k -> end.x - k -> begin.x);
	}
	b = (double)-(a * k -> end.x) + (double)(k -> end.y);
	printf("%f\n", a);
}*/

int biggest(int a, int b)
{
	if(a > b) return a;
	else return b;
}
