#include <stdio.h>

typedef enum { false, true }bool;

void tidy_significands(int *data_array, int size);
void print_array(int *data_array, int size, int number, bool IO);

int main()
{
	int array1[8] = { 0, 0, 0, 0, 0, 743, 2489, 62 };
	int array2[5] = { 0, 0, 0, 0, 0 };
	int array3[7] = { 0, 0, 15, 17, 19, 21, 23 };
	int array4[9] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	
	print_array(array1, sizeof(array1) / sizeof(int), 1, true);
	tidy_significands(array1, sizeof(array1) / sizeof(int));
	print_array(array1, sizeof(array1) / sizeof(int), 1, false);

	print_array(array2, sizeof(array2) / sizeof(int), 2, true);
	tidy_significands(array2, sizeof(array2) / sizeof(int));
	print_array(array2, sizeof(array2) / sizeof(int), 2, false);

	print_array(array3, sizeof(array3) / sizeof(int), 3, true);
	tidy_significands(array3, sizeof(array3) / sizeof(int));
	print_array(array3, sizeof(array3) / sizeof(int), 3, false);

	print_array(array4, sizeof(array4) / sizeof(int), 4, true);
	tidy_significands(array4, sizeof(array4) / sizeof(int));
	print_array(array4, sizeof(array4) / sizeof(int), 4, false);

	return 0;
}

void tidy_significands(int *data_array, int size)
{
	int i, temp, teller = 0;
	for(i = 0; i < size; i++)
	{
		if(*(data_array + i) > 0)
		{
			temp = *(data_array + teller);
			*(data_array + teller) = *(data_array + i);
			*(data_array + i) = temp;
			teller++;
		}
	}
}

void print_array(int *data_array, int size, int number, bool IO)
{
	int i;
	if(IO) printf("Array %d %s\t: {", number, "input");
	else printf("Array %d %s\t: {", number, "output");
	for(i = 0; i < size; i++)
	{
		printf("%5d,", *(data_array + i));
	}
	printf(" }\n");
	if(!IO) printf("\n");
}
