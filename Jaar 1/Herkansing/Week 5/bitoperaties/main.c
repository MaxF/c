#include <stdio.h>

typedef enum {nul = 0, een} bitje;

bitje bepaal_derde_bit(int getal);

int main()
{
	int getal;
	printf("Geef een getal: ");
	scanf("%d", &getal);
	printf("Het derde bit van rechts van getal %d = %d\n", getal, bepaal_derde_bit(getal));
}

bitje bepaal_derde_bit(int getal)
{
	getal = getal >> 2;
	bitje bit = getal & 1;
	return bit;
}
