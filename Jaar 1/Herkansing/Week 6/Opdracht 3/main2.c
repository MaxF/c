#include <stdio.h>
#include <stdlib.h>

int strlen(char *s);
char *create_concatenated_cstring(char *source1, char *source2);
void destroy_cstring(char **gluedstring);

int main()
{
	char *string1 = "Common sense is genius ";
	char *string2 = "dressed in its working clothes.";
	char *together = create_concatenated_cstring(string1, string2);
	printf("Aan elkaar geplakt vormen de strings de volgende " \
		"quote:\n\n\"%s\"\n\n", together);
	destroy_cstring(&together);
	if(NULL == together)
		printf("De string was inderdaad vernietigd!\n");
	else
		printf("Het vernietigen van de string is mislukt!\n");
	return 0;
}

int strlen(char *s)
{
	int n = 0;
	for(n = 0; *s != '\0'; s++, n++);
	return n;
}

char *create_concatenated_cstring(char *source1, char *source2)
{
	int i, j, length1 = strlen(source1), length2 = strlen(source2), totalLength = length1 + length2;
	char *combined = (char *)malloc(sizeof(char) * totalLength);
	for(i = 0; i < length1; i++) combined[i] = source1[i];
	for(j = 0; j < length2; j++) combined[j + i]  = source2[j];
	return &combined[0];
}

void destroy_cstring(char **gluedstring)
{
	free(*gluedstring);
	*gluedstring = NULL;
}
