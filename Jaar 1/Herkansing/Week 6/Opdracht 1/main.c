#include <stdio.h>

typedef enum { nul = 0, een } bitje;

bitje bepaal_derde_bit(int getal);

int main()
{
	int number1 = 33, number2 = 127, number3 = 128, number4 = -940;
	printf("Het derde bit van rechts van getal %4d = %d\n", number1, bepaal_derde_bit(number1));
	printf("Het derde bit van rechts van getal %4d = %d\n", number2, bepaal_derde_bit(number2));
	printf("Het derde bit van rechts van getal %4d = %d\n", number3, bepaal_derde_bit(number3));
	printf("Het derde bit van rechts van getal %4d = %d\n", number4, bepaal_derde_bit(number4));
	return 0;
}

bitje bepaal_derde_bit(int getal)
{
	bitje bit;
	getal = getal << 29;
	getal = getal >> 31;
	bit = getal & 1;
	return bit;
}
