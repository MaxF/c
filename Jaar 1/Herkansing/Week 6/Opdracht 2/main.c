#include <stdio.h>

#define SIZE 5

typedef enum { false, true } bool;

void fillArray(int *numbers);
void printArray(int *numbers);

int main()
{
	int numbers[SIZE] = { 0, 0, 0, 0, 0 };
	fillArray(numbers);
	printArray(numbers);
	printf("%d\n", numbers[0]);
	return 0;
}

void fillArray(int *numbers)
{
	int temp, i, j;
	bool stopped = false;
	while(1)
	{
		for(i = 0; i < SIZE; i++)
		{
			printf("Geef het volgende getal <0 = stoppen>: ");
			scanf("%d", &temp);
			if(temp == 0) 
			{
				printf("\n");
				stopped = true;			
				break;
			}
			else 
			{
				for(j = 0; j < (SIZE - 1); j++)
				{
					numbers[j] = numbers[j + 1];
				}
				numbers[SIZE - 1] = temp;
			}
		}
		if(stopped) break;
	}
}

void printArray(int *numbers)
{
	int i;
	for(i = 0; i < SIZE; i++) printf("Getal %d is: %d\n", (i + 1), numbers[i]);
}
