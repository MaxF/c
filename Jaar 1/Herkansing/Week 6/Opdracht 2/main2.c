#include <stdio.h>

typedef enum { false, true } bool;

void check(int number, int *numbers, int *numbers_checked, int size);

int main()
{
	int getallen[] = {1, 5, 4, 7, 1, 4, 1, 7, 5, 5, 3, 1};
	int checked[] = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
	int i, size = sizeof(getallen) / sizeof(int);
	for(i = 0; i < size; i++)
	{
		check(getallen[i], getallen, checked, size);
	}
	return 0;
}

void check(int number, int *numbers, int *numbers_checked, int size)
{
	int i, position = 0, amount = 0;
	bool exists = false;

	for(i = 0; i < size; i++)
	{
		if(number == numbers_checked[i]) exists = true;
	}
	if(!exists)
	{
		while(numbers_checked[position] != 10) position++;
		numbers_checked[position] = number;
		for(i = 0; i < size; i++)
		{
			if(number == numbers[i]) amount++;
		}
		printf("Het aantal voorkomens van %d is: %d\n", number, amount);
	}
}


