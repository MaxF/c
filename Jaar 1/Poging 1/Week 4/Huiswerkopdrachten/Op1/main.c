#include <stdio.h>
#include <stdlib.h>

#define AANTAL_STUDIES	3

typedef struct { int id; char *naam; char *studie; } student;

student *dupliceer(student *p, unsigned int n);

int main()
{
	int i;
	student *duplicaten;
	student a = {110, "Harry de Hond", "Computer Engineering"};

	//Hoe groot is deze struct in het geheugen?
	printf("De omvang van de student-struct a is: %i bytes\n", sizeof(a));

	//Harry de Hond gaat AANTAL_STUDIES verschillende studies volgen:
	duplicaten = dupliceer(&a, AANTAL_STUDIES);
	if(duplicaten == NULL)
	{
		return 0;
	}

	duplicaten[1].studie = "History";
	//Alternatieve schrijfwijze: pijlnotatie!
	(duplicaten + 2)->studie = "Artificial Intelligence";

	printf("De array bevat de volgende duplicaten:\n");
	for(i = 0; i < AANTAL_STUDIES; i++)
	{
		printf(" > Student ID %i met naam %s studeert %s\n", duplicaten[i].id, duplicaten[i].naam, duplicaten[i].studie);
	}

	free(duplicaten);
	duplicaten = NULL;

	return 0;
}

student *dupliceer(student *p, unsigned int n)
{
	student *duplicate = (student*)malloc(sizeof(student)*n);
	for(int i = 0; i < n; i++)
	{
		duplicate[i] = *p;

	}
	return duplicate;
}
