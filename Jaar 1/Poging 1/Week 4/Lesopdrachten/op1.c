#include <stdio.h>
#include <stdlib.h>

void string_heapcopy(const char *source, char *target);
void string_destroy(char *koe);
size_t strlen(const char *s);
void replace_first(char *str, char t, char r);

int main()
{
    const int BUFF_SIZE = 1024;
    char str[BUFF_SIZE];
    char *copy = NULL; // empty var on stack called 'copy'
    
    printf("Enter a string        : ");
    fgets(str, BUFF_SIZE, stdin);
    replace_first(str, '\n', '\0'); // fgets adds '\n', remove it here!
    str[BUFF_SIZE - 1] = '\0'; // For security purposes
     
    //printf("Contents of original (cstr) : %s\n", str);
    
	string_heapcopy(str, copy);
    
    printf("Contents of duplicate : %s\n", copy);
    printf("Address of inputstring: %p\n", str);
    printf("Address of duplicate  : %p\n", copy);

    string_destroy(copy);
    string_destroy(copy);
    
    return 0;
}

void string_heapcopy(const char *source, char *target)
{
	int i;
	// stringlengte bepalen
	size_t n = strlen(source);
	// target aanvragen in de heap!
	// malloc verwacht aantal bytes als size_t
	target = (char *)malloc(sizeof(char) * (n + 1));
	// target karakter voor karakter vullen met data uit source (loop)
	for (i = 0; i < (n + 1); i++)
	{
		target[i] = source[i];
	}
}

void string_destroy(char *koe)
{
    if (NULL == koe)
    {
        printf("This string was already destroyed!\n");
        return;
    }

    free(koe);
    koe = NULL;
}


size_t strlen(const char *s) // "koei\0"
{
	size_t n = 0;
	for (n = 0; *s != '\0'; s++, n++);
	return n;
}

void replace_first(char *str, char t, char r)
{
	// zolang karakters in string
	// > als c (huidige karakter) == t
	//   - c = r
	//   - stop
	while (*str != '\0')
	{
		if (*str == t)
		{
			*str = r;
			break;
		}
		str++;
	}
}
