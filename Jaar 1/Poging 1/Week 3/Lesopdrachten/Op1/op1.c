#include <stdio.h>

void kwadrateer_by_ref(int *p);

int main()
{
	int p = 8947;
	int a = p;
	kwadrateer_by_ref(&p);
	printf("\nHet kwadraat van %d is %d.\n\n",a, p);
	return 0;
}

void kwadrateer_by_ref(int *p)
{
	*p = *p * *p;
}
