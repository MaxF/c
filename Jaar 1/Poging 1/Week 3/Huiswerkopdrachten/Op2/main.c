#include <stdio.h>

void reverse(char *cstr);

int main()
{
		char a[100];
		printf("\nGeef een string (max 100 tekens): ");
		fgets(a, 100, stdin);
		reverse(a);
		printf("%s\n", a);
		return 0;
}

void reverse(char *cstr)
{
	int i;
	int teller = 0;
	for(i = 0; i < 100; i++) //Kijk hoe veel tekens er in cstr zitten
	{
		if(cstr[i] == '\0')
		{
			break;
		}
		teller++;
	}
	char b[teller];
	for(i = 0; i <= teller; i++)
	{
		b[i] = cstr[i];
	}

	b[teller] = '\0';
	int count = 0;
	for(i = teller; i >= 0; i--)
	{
		cstr[count] = b[i - 1];
		count++;
	}
	cstr[teller] = '\0';
}
