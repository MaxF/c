#include <stdio.h>
typedef enum { true, false} bool;

bool palindroom(char *cstr, int grootte);
int sizeCharArray(char *point, int grootte);

int main()
{
		printf("Geef een string van maximaal 100 karakters: ");
		char a[100];
		fgets(a, 100, stdin);
		if(palindroom(a, sizeCharArray(a, 100)) == true)
		{
				printf("Dit is een palindroom\n");
		}
		else
		{
				printf("Dit is geen palindroom\n");
		}
		return 0;
}

bool palindroom(char *cstr, int grootte)
{
		bool value;
		for(int i = 0; i < (grootte / 2); i++)
		{
			if(cstr[i] == cstr[(grootte - 1) - i])
			{
				value = true;
			}
			else
			{
				value = false;
				break;
			}
		}
		return value;
}

int sizeCharArray(char *point, int grootte)
{
		int teller = 0;
		for(int i = 0; i < grootte; i++)
		{
			if(point[i] == '\0')
			{
					break;
			}
			teller++;
		}
		return teller - 1;
}