#include <stdio.h>
typedef enum { true, false } bool;

bool deel_geheel(unsigned int teller, unsigned int noemer, unsigned int *quotient, unsigned int *rest);

int main()
{
		unsigned int teller, noemer, quot = 0, res = 0;
		printf("Geef de teller: ");
		scanf("%u", &teller);
		printf("\nGeef de noemer: ");
		scanf("%u", &noemer);
		if(deel_geheel(teller, noemer, &quot, &res) == true)
		{
			printf("\n%d / %d = %d rest %d\n", teller, noemer, quot, res);
		}
		else
		{
			printf("AUB niet delen door 0, heeft geen zin.\n");
		}
		return 0;
}

bool deel_geheel(unsigned int teller, unsigned int noemer, unsigned int *quotient, unsigned int *rest)
{
		if(noemer == 0)
		{
				return false;
		}
		else
		{
				*quotient = teller / noemer;
				*rest = teller % noemer;
				return true;
		}
}
