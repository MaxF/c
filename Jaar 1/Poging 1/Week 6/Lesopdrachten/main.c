#include <stdio.h>
#define MIN_X -39
#define MIN_Y -12
#define MAX_X 40
#define MAX_Y 12

int main()
{
	//const int MIN_X = 1;
	//const int MIN_Y = 1;
	//const int MAX_X = 80;
	//const int MAX_Y = 25;
	int rij, kol;

	for(rij = MAX_Y; rij >= MIN_Y; rij--)
	{
		for(kol = MIN_X; kol <= MAX_X; kol++)
		{
			if(rij == 0 && kol == 0)
			{
				printf("+");
			}
			else if(rij == 0)
			{
				printf("-");
			}
			else if(kol == 0)
			{
				printf("|");
			}
			else
			{
				printf(" ");
			}
		}
	}
	return 0;
}
