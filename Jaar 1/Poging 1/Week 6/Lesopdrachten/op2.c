#include <stdio.h>
#include <math.h>

#define MIN_Y -17
#define MAX_Y 18
#define MIN_X -49
#define MAX_X 50

typedef struct {
	int x;
	int y;
} point;

typedef struct {
	point p;
	point q;
} line;

void plot_line(line *k);

int main()
{
	point a = {-25, 7};
	point b = {30, 5};
	line t = {a, b};
	plot_line(&t);
	return 0;
}

void plot_line(line *k)
{
	int row, col;
	// y = a * x + b
	// a = delta_y / delta_x
	// b = (y / x) - a
	double a = (double)(k->p.y - k->q.y) / (double)(k->p.x - k->q.x);
	double b = ((double)k->p.y / (double)k->p.x) - a;

	for(row = MAX_Y; row >= MIN_Y; row--)
	{
		for(col = MIN_X; col <= MAX_X; col++)
		{
			if(row == (int)round(a * (double)col + b))
			{
				printf("*");
			}
			else
			{
				printf(" ");
			}
		}
	}
}
