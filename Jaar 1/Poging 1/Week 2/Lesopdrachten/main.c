#include <stdio.h>
#include "wis.h"

int main()
{
		double input [] = {1.0, 2548.27, 5.3333, -783.58};
		int arrayLength = sizeof(input) / sizeof(double);
		double maximum = max(input, arrayLength);
		printf("\nMax gevonden waarde is %g\n\n", maximum);
		return 0;
}
