#include <stdio.h>
typedef struct opdrNemers{ char voornaam[50]; char achternaam[50]; int leeftijd; double uurtarief; int uren; } opdrachtnemer;

void rapporteerBeloningen(opdrachtnemer opdrNemers[], int s);

int main()
{
		opdrachtnemer opdrNemers[] = 
		{
				{"Koos", "Korswagen", 32, 135.75, 89},
				{"Bolus", "Bonkpok", 44, 162.25, 24},
				{"Bonkige", "Harry", 26, 89.45, 46}
		};
		int s = sizeof(opdrNemers)/sizeof(opdrachtnemer);
		rapporteerBeloningen(opdrNemers, s);
		return 0;
}
