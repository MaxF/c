#include "ext.h"
#include <stdio.h>

void showBinary(int numb)
{
		printf("\nHet getal %d is binair: ", numb);
		int binaryNumber[32];
		for(int i = 31; i > -1; i--)
		{	
			binaryNumber[i] = numb % 2;
			numb /= 2;
		}
		for(int i = 0; i < 32; i++)
		{
				printf("%d", binaryNumber[i]);
				if((i + 1) % 4 == 0)
				{
						printf(" ");
				}
		}
		printf("\n\n");
}

void compareBinary(int getal1, int getal2)
{
		int getal3, aantal = 0, binaryNumber[32];
		printf("\nXOR van beide getallen: ");
		getal3 = (getal1 ^ getal2);
		for(int i = 31; i > -1; i--)
		{
				binaryNumber[i] = getal3 % 2;
				getal3 /= 2;
		}
		for(int i = 0; i < 32; i++)
		{
				if(binaryNumber[i] == 1)
				{
					printf(" ");
				}
				else
				{
					printf("X");
					aantal++;
				}
				if((i + 1) % 4 == 0)
				{
					printf(" ");
				}
		}
		printf("\n\nBeide getallen komen op %d bitposities overeen!\n\n", aantal);
}
