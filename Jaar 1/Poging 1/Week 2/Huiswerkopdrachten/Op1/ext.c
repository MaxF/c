#include "ext.h"
#include <stdio.h>

void showBinary(int numb)
{
		printf("\nHet getal %d is binair: ", numb);
		int remainder, i = 1;
		long long binaryNumber = 0;
		while(numb != 0)
		{	
			remainder = numb % 2;
			numb = numb / 2;
			binaryNumber = binaryNumber + (remainder * i);
			i = i * 10;
		}
		printf("%lld.\n\n", binaryNumber);
}
