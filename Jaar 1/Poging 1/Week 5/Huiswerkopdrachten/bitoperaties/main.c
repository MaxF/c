#include <stdio.h>

typedef enum {
	nul,
	een 
} bitje;

bitje bepaal_derde_bit(int getal);

int main()
{
	int getal_1 = 33;
	int getal_2 = 127;
	int getal_3 = 128;
	int getal_4 = -940;
	
	printf("Het derde bit van rechts van getal 33 = %d\n", bepaal_derde_bit(getal_1));
	printf("Het derde bit van rechts van getal 127 = %d\n", bepaal_derde_bit(getal_2));
	printf("Het derde bit van rechts van getal 128 = %d\n", bepaal_derde_bit(getal_3));
	printf("Het derde bit van rechts van getal -940 = %d\n", bepaal_derde_bit(getal_4));

	return 0;
}

bitje bepaal_derde_bit(int getal)
{
	getal = getal >> 2;
	return getal & 1;
}
