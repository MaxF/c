#include <stdio.h>

#define MIN_X -39
#define MIN_Y -12
#define MAX_X 40
#define MAX_Y 12

void plot_grid();

int main()
{
	plot_grid();
	return 0;
}

void plot_grid()
{
	int i, j, k;
	
	for(i = MIN_Y; i < MAX_Y; i++)
	{
		if(i != 0)
		{
			for(j = MIN_X; j < 0; j++)
			{
				printf(" ");
			}
			printf("|\n");
		}
		else
		{
			for(k = MIN_X; k < MAX_X; k++)
			{
				if(k == 0)
				{
					printf("+");
				}
				else
				{
					printf("-");
				}
			}
			printf("\n");
		}
	}
}
