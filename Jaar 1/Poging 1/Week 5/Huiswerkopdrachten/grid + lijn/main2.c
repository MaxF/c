#include <stdio.h>
#include <math.h>

#define MIN_X -39 
#define MIN_Y -12
#define MAX_X 40
#define MAX_Y 12

#define MAX(a, b)(a > b ? a : b)
#define MIN(a, b)(a < b ? a : b)

typedef struct {
  int x;
  int y;
}
point;

typedef struct {
  point a;
  point b;
}
line;

void plot_grid();
void plot_line(line * k);

int main() {
  line line1;
  line1.a.x = -25;
  line1.a.y = -7;
  line1.b.x = 30;
  line1.b.y = 5;
  plot_line( & line1);
  return 0;
}

void plot_line(line * k) {
  int topx, y, ypunt, i, j;
  double a, b;
  topx = MAX(k -> a.x, k -> b.x);
  //topy = MAX(k->a->y, k->b->y);
  //minx = MIN(k->a->x, k->b->x);
  //miny = MIN(k->a->y, k->b->y);

  if (topx == k -> a.x) {
    a = (double)(k -> a.y - k -> b.y) / (double)(k -> a.x - k -> b.x);
  } else {
    a = (double)(k -> b.y - k -> a.y) / (double)(k -> b.x - k -> a.x);
  }

  //a = (double)(k->b.y - k->a.y) / (double)(k->b.x - k->a.x);
  //printf("%f\n", a);

  b = ((double)(-(double) a) * k -> a.x) + k -> a.y;

  for (i = MAX_Y; i >= MIN_Y; i--) {
    for (j = MIN_X; j <= MAX_X; j++) {
      ypunt = round((a * j) + b);

      if (i == ypunt) {
        printf("*");
      } else if (i == 0 && j == 0) {
        printf("+");
      } else if (j == 0) {
        printf("|");
      } else if (i == 0) {
        printf("-");
      } else {
        printf(" ");
      }
    }
    printf("\n");
  }

}

void plot_grid() {
/*
  x = MAX_X - MIN_X;
  xnul = x / 2;

  y = MAX_Y - MIN_Y;
  ynul = y / 2;

  for (i = 0; i < y; i++) {
    if (i == ynul) {
      for (k = 0; k < x; k++) {
        if (k == xnul) {
          printf("+");
        } else {
          printf("-");
        }
      }
      printf("\n");
    } else {
      for (j = 0; j < xnul; j++) {
        printf(" ");
      }
      printf("|\n");
    }
  }
*/
}
