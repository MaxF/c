#include <stdio.h>

void tidy_significands(int *data_array, int size);

int main()
{
	int data [] = {0, 0, 0, 0, 0, 743, 2489, 62};
	int grootte = sizeof(data)/sizeof(int);
	tidy_significands(data, grootte);
	return 0;
}

void tidy_significands(int *data_array, int size)
{
	int i, c;
	printf("Array 1 input: {");
	for(i = 0; i < size; i++)
	{
		printf(" %d,", data_array[i]);
	}
	printf("}\n");
	for(int i = 0; i < size; i++)
	{
		if(data_array[i] != 0)
		{
			for(c = 0; c < size; c++)
			{
				if(data_array[c] == 0)
				{
					data_array[c] = data_array[i];
					data_array[i] = 0;
					break;
				}
			}
		}
	}
	printf("Array 1 output: {");
	for(int j = 0; j < size; j++)
	{
		printf(" %d,", data_array[j]);
	}
	printf("}\n");
}
