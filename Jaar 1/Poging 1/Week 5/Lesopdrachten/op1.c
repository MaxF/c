#include <stdio.h>

int biggest_value(int *begin, int *end);

int main()
{
	int testset1[] = {9, 7, 1, 6, 8, 3, 4, 2, 0};
	int testset2[] = {6, 6, 6, 6, 6, 6, 6, 6, 6};
	int grootste;
	grootste = biggest_value(&testset1[0], &testset1[9]);
	printf("Het grootste getal in de eerste testset is %i\n", grootste);

	grootste = biggest_value(&testset2[0], &testset2[9]);
	printf("Het grootste getal in de tweede testset is %i\n", grootste);

	return 0;
}

int biggest_value(int *begin, int *end)
{
	int biggest;
	//Vergelijk eerste met laatste
	//Als eerste groter dan laatste, sla eerste op als grootste
	if(*begin > *end)
	{
		biggest = *begin;
	}
	else
	{
		biggest = *end;
	}
	begin++;
	end--;
	while(!(begin >= end))
	{
			if(*begin > biggest)
			{
				biggest = *begin;
			}
			//Sla laatste op als grootste
			else if(*end > biggest)
			{
				biggest = *end;
			}
			begin++;
			end--;
	}
	return biggest;
}
