/* Schrijf en demonstreer een functie die bij een gezochte positieve integer waarde de index binnen een integer array (van alleen positieve getallen) teruggeeft.
De functie neemt als argumenten de te doorzoeken array, de arraylengte en de gezochte waarde. 
Als returnvalue wordt de index teruggegeven van de eerst gevonden overeenkomende waarde, of -1 als de waarde niet in de array gevonden wordt. 
Bij het demonstreren van de werking van deze functie in main() maak je uiteraard gebruik van passende ‘if’-statements die onderscheiden of een waarde gevonden is, en zo ja, op welke positie. */

#include <stdio.h>
int numbersarray [] = {5, 745, 23, 8, 234, 6, 3, 25445, 76}; //Declareren array
int arraylength = sizeof(numbersarray) / sizeof(int); //De grootte van de array wordt in een integer gestopt
int wantednumber = 5; //Het gezochte nummer

int zoekIndex(int numbers[], int arrayLength, int wantedNumber); //Declareren functie

int main()
{
		int Result = zoekIndex(numbersarray, arraylength, wantednumber); //Vraag de index van het te zoeken getal op
		if(Result != -1) //Geeft het resultaat als het getal gevonden is
		{
				printf("\n\nDe index van het gezochte getal %d, is %d.\n\n", wantednumber, Result);
		}
		else //Geeft de melding dat het getal niet in de array gevonden is
		{
				printf("\n\nHet gezochte getal %d, is niet in de array gevonden.\n\n", wantednumber);
		}

		return 0;
}

int zoekIndex(int numbers [], int arrayLength, int wantedNumber)
{
		int result = -1; //Declareren integer waar het resultaat ingezet wordt met als default value de status dat het getal niet gevonden is

		for(int i = 0; i < arrayLength; i++)
		{
				if(wantedNumber == numbers[i])
				{
						result = i;
						result++;
				}
		}
		return result -1;
}
