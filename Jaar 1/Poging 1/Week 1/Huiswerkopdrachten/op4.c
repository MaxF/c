/* 4. Schrijf en demonstreer een functie die een array van lowercase characters (datatype ‘char’) sorteert op alfabetische volgorde en het sorteerresultaat afdrukt op het scherm. 
De functie neemt als argumenten de te doorzoeken array en de arraylengte en heeft geen return value. 
Let erop dat een ‘char’ net als een ‘int’ gewoon een numeriek type is. 
De nummers 97 t/m 122 staan voor de kleine letters a – z in de ascii tabel. 
Gebruik dat als basis voor je sorteervergelijking. */

#include <stdio.h>

char Letters[] = {'r', 'a', 's', 'i', 'y', 'a', 'q'}; //Array met de te sorteren letters
//char Letters[] = {"rasiyaq"}; //Array met de te sorteren letters
int arrayLength = sizeof(Letters) / sizeof(char); //Lengte van de array wordt in een integer opgeslagen

void sortAlpha(char letters[], int arraylength); //Declareren functie voor het sorteren op alfabet

int main()
{
		sortAlpha(Letters, arrayLength);
		return 0;
}

void sortAlpha(char letters[], int arraylength)
{
		int a = 97;
		int z = 122;
		int teller = 0;

		int ascii[arraylength]; //Declareer lege integer array waar de ascii-nummer in komen te staan
		int asciiSorted[arraylength];

	 	for(int i = 0; i < arraylength; i++) //Zet de letters om in ascii-nummers
		{
			ascii[i] = letters[i];
		}
		for(int i = a; i < z; i++) //Sorteer de getallen van klein naar groot
		{
			for(int j = 0; j < arraylength; j++)
			{
				if(i == ascii[j])
				{ 
					asciiSorted[teller] = ascii[j];
					teller++;
				}
			}
		}
		printf("\n\nDe letters gesorteerd op alfabet:");
		for(int i = 0; i < arrayLength; i++) //Laat de getallen als ascii-teken zien
		{	
				printf(" %c", asciiSorted[i]);
		}
		printf("\n\n");
}
