/*Schrijf een programma waarin een gegeven array van integers wordt afgedrukt, omgedraaid en opnieuw wordt afgedrukt. Je mag hierbij meerdere arrays gebruiken. Omdat in C functies geen arrays kunnen returnen, hoef je hier geen aparte functie voor te schrijven (alles mag in main).*/

#include <stdio.h>
#define arrayLength sizeof(numbers) / sizeof(int) //zorgt ervoor dat arrayLength als getal in te vullen is voor de lengte van numbers2

int numbers[] = {4, 84, 2, 93, 28, 945}; //Declareren array
int numbers2[(int) arrayLength]; //Declareren array met dezelfde lengte als numbers

int main ()
{
		printf("\nInhoud van de array:");

		for(int i = 0; i < arrayLength; i++) //laat de inhoud van de array zien
		{
				printf(" %d", numbers[i]);
		}
		printf(".\n\nInhoud van de vorige array omgekeerd:");
		for(int i = 0; i < arrayLength; i++) //draait de volgorde van de inhoud van de array om
		{
				numbers2[arrayLength - (i + 1)] = numbers[i];
		}
		for(int i = 0; i < arrayLength; i++) //laat de inhoud van de omgekeerde array zien
		{
				printf(" %d", numbers2[i]);
		}
		printf("\n\n");
		return 0;
}
