/*
1. Schrijf en demonstreer een functie die een array van integers van een opgegeven lengte onderzoekt om de drie grootste getallen eruit te pikken en deze op het scherm weer te geven (met printf()). Let erop dat je moet controleren of een array wel minimaal 3 elementen bevat. Deze functie hoeft geen return value te hebben.
*///

#include <stdio.h>

int main()
{
	int getallen[] = {45, 100, 86, 83, 92, 44}; //aanmaken array met getallen
	int biggest[] = {0, 0, 0}; //aanmaken array met getallen die vergeleken gaan worden
	int arrayLength = sizeof(getallen) / sizeof(int); //de lengte van de array wordt als integer opgeslagen

	if(arrayLength >= 3) //check of de array uit minimaal 3 getallen bestaat
	{
		for(int i = 0; i < arrayLength; i++) 
		{
			if(biggest[0] < getallen[i]) //zoeken naar het grootste getal
			{
				biggest[0] = getallen[i];
			}
		}
		for(int i = 0; i < arrayLength; i++)
		{

			if(biggest[1] < getallen[i] && getallen[i] < biggest[0]) //zoeken naar het op een na grootste getal
			{
				biggest[1] = getallen[i];
			}
		}
		for(int i = 0; i < arrayLength; i++)
		{
			if(biggest[2] < getallen[i] && getallen[i] < biggest[0] && getallen[i] < biggest[1]) //zoeken naar het op twee na grootste getal
			{
				biggest[2] = getallen[i];
			}
		}
		printf("\nDe 3 grootste getallen gesorteerd van groot naar klein zijn: %d, %d en %d.\n\n", biggest[0], biggest[1], biggest[2]);
		return 0;
	}
	else
	{
			printf("\nDe array bestaat uit minder dan 3 getallen\n\n"); //geeft een waarschuwing wanneer er minder dan 3 getallen in de array staan
	}
}
//testt
