#include <stdio.h>

void copy_cstring(char *original, char *copy);

int main()
{
	char origineel[50]; 
	char kopie[50]; 
	printf("Geef een string om te kopieren:\n> "); 
	scanf("%s", origineel); 
	copy_cstring(origineel, kopie); 
	printf("\n"); 
	printf("\'kopie\' bevat de volgende string: %s\n", kopie); 
	return 0; 
}

void copy_cstring(char *original, char *copy)
{
	int i;
	for(i = 0; i < 50; i++, original++, copy++)
	{
		*copy = *original;
	}
}
