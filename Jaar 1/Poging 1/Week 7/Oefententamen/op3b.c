#include <stdio.h>
#include <stdlib.h>

char *create_concatenated_cstring(char *String1, char *String2);
void destroy_cstring(char **gluedstring);

int main()
{
	char *string1 = "Common sense is genius ";
	char *string2 = "dressed in its working clothes";
	char *together = create_concatenated_cstring(string1, string2);
	printf("Aan elkaar geplakt vormen de strings de volgende quote:\n\n\"%s\"\n\n", together);
	destroy_cstring(&together);
	if(NULL == together)
	{
		printf("De string is inderdaad vernietigd!\n");
	}
	else
	{
		printf("De string is niet vernietigd!\n");
	}
}

char *create_concatenated_cstring(char *String1, char *String2)
{
	int length1 = 0, length2 = 0, i;
	while(String1[length1] != '\0')
	{
		length1++;
	}
	while(String2[length2] != '\0')
	{
		length2++;
	}
	char *mergedString = (char*)malloc(sizeof(char)*(length1 + length2));
	for(i = 0; i < length1; i++)
	{
			mergedString[i] = String1[i];
	}
	for(i = 0; i < length2; i++)
	{
		mergedString[(i + length1)] = String2[i];
	}
	return mergedString;
}

void destroy_cstring(char **gluedstring)
{
	free(*gluedstring);
	*gluedstring = NULL;
}
