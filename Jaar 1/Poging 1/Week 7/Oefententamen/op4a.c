#include <stdio.h>

typedef struct{   
	int dag;
	int maand;
} kalender_dag;

typedef enum
{
	januari = 1,
	februari,
	maart,
	april,
	mei,
	juni,
	juli,
	augustus,
	september,
	oktober,
	november,
	december,
} maanden;

typedef struct{
	int aantal_dagen;
} timestamp;

int kalenderdag_to_timestamp(kalender_dag *k);

int main()
{
	kalender_dag a = {17, oktober};
	kalender_dag b = {12, januari};
	kalender_dag *dag = &a;
	kalender_dag *dag2 = &b;
	int dag_1 =	kalenderdag_to_timestamp(dag2);
	int dag_2 = kalenderdag_to_timestamp(dag);
	printf("%d", (dag_1 + dag_2));
	return 0;
}

int kalenderdag_to_timestamp(kalender_dag *k)
{
	int aantal_dagen = 0, i;
	for(i = 0; i < ((k->maand) - 1); i++)
	{
		if(k->maand == februari)
		{
			aantal_dagen += 28;
		}
		else if(k->maand == augustus)
		{
			aantal_dagen += 31;
		}
		else
		{
			if((k->maand % 2) == 1)
			{
				aantal_dagen += 31;
			}
			else
			{
				aantal_dagen += 30;
			}
		}
	}
	aantal_dagen += k->dag;
	return aantal_dagen;
}
