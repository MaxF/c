#include <stdio.h>

void check(int *getal, int *numbers, int *numbers_checked, int grootte);
void print_results(int *numbers_checked, int grootte);

int main()
{
	int getallen[] = {1, 5, 4, 7, 1, 4, 1, 7, 5, 5, 3, 1};
	int getallen_checked [] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int i, size = sizeof(getallen)/sizeof(int), size_checked = sizeof(getallen_checked)/sizeof(int);

	for(i = 0; i < size; i++)
	{
		int *p = &getallen[i];
		check(p, getallen, getallen_checked, size);
	}
	print_results(getallen_checked, size_checked);
	return 0;
}

void check (int *getal, int *numbers, int *numbers_checked, int grootte)
{
	int i;
	if(numbers_checked[*getal] == 0)
	{
		for(i = 0; i < grootte; i++)
		{
			if(*getal == numbers[i])
			{
				numbers_checked[*getal]++;
			}
		}
	}
}

void print_results(int *numbers_checked, int grootte)
{
	int i;
	for(i = 0; i < grootte; i++)
	{
		if(numbers_checked[i] != 0)
		{
			printf("Het aantal voorkomens van %d is: %d\n", i, numbers_checked[i]);
		}
	}
}
