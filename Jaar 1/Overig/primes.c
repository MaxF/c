#include <stdio.h>
#include <stdbool.h>

bool check_prime(unsigned int number);

int main()
{
	FILE *fp;
	fp = fopen("primes.txt", "w");
	unsigned int totalPrimes = 100000, currentNumber, i = 1;
	while(1)
	{	while(i < totalPrimes + 1)
		{
			if(check_prime(currentNumber)) 
			{
				fprintf(fp, "Priemgetal %d: %d\n", i, currentNumber);
				printf("Log: Prime %d/%d found!\n", i, totalPrimes);
				i++;
			}
			currentNumber++;
		}
		break;
	}
	fclose(fp);
    	return 0;
}

bool check_prime(unsigned int number)
{
    int i;
    if(number <= 1) // 1 en alles daaronder is natuurlijk geen priemgetal!
    {
        return false;
    }
    for(i = 2; i < (number - 1); i++) // Loop zo vaak als ('number' - 1) groot is vanaf 2 
    {
        if(number % i == 0) // Kijk of 'number' deelbaar is door 'i'
        {
            return false; 
	}
    }
    return true; // Exit en geef de waarde 1 mee
}


