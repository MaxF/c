#include <stdio.h>
unsigned short construeer_transmissie(unsigned short scores[], size_t size);
int main(){
	unsigned short scores[] = {11,7,0,4};
	int i;
	for(i=0;i<4;i++)
	{
		printf("Speler %i heeft\t %i punten\t (HEX 0x%X)\n",i,scores[i],scores[i]);
	}
	unsigned short hex = construeer_transmissie(scores, 4);
	printf("Ingepakt is de bijbehorende transmissie: 0x%X",hex);
	return 0;
}
unsigned short construeer_transmissie(unsigned short scores[], size_t size){
	unsigned short transmissie = 0;
	size_t i;
	for(i = 0; i<size; i++){
		transmissie = transmissie << 4; //Elke keer 4 bits naar links shiften om ruimte te maken voor de nieuwe bits
		transmissie = transmissie | scores[i]; //Scores rechts plaatsen.
	}
	return transmissie;
}
