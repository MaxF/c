#include <stdio.h>

#define AANTAL_KOEIEN	3

typedef struct {
	short leeftijd;
	int aantal_uiers;
	double gewicht_in_kg;
} koe;

double gemiddeld_koe_gewicht(koe **koeienlijst, size_t aantal_koeien);

int main(void)
{
	koe koe1 = {7, 2, 453.26};
	koe koe2 = {4, 3, 511.51};
	koe koe3 = {5, 1, 418.78};

	koe *koecollectie[AANTAL_KOEIEN] = {&koe1, &koe2, &koe3};
	double gem_gewicht = gemiddeld_koe_gewicht(koecollectie, AANTAL_KOEIEN);
	printf("\nHet gemiddelde gewicht van een koe in de lijst is: %g kg.\n\n",
		gem_gewicht);

	return 0;
}

double gemiddeld_koe_gewicht(koe **koeienlijst, size_t aantal_koeien){
	size_t i;
	double gewicht=0;
	for(i=0;i<aantal_koeien;i++){
		gewicht += koeienlijst[i]->gewicht_in_kg;
	}
	return (gewicht/aantal_koeien);
}
