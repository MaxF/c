#include <stdio.h>
#include <stdbool.h>

bool strcomp(char *str1, char *str2);

int main(void)
{
	char string1[] = "Koeien op stal";
	char string2[] = "Koeien op stal";
	char string3[] = "Gloeiende bospinda";
	char string4[] = "Gloeiende bospinda ";
	if(strcomp(string1,string2)){
		printf("De string \"%s\"\tis EXACT hetzelfde als de string \"%s\"\n",string1,string2);
	}else{
		printf("De string \"%s\"\tis NIET hetzelfde als de string \"%s\"\n",string1,string2);
	}
	if(strcomp(string3,string4)){
		printf("De string \"%s\"\tis EXACT hetzelfde als de string \"%s\"\n",string3,string4);
	}else{
		printf("De string \"%s\"\tis NIET hetzelfde als de string \"%s\"\n",string3,string4);
	}
	
	
	return 0;
}

bool strcomp(char *str1, char *str2)
{
   int c = 0;
 
   while (str1[c] == str2[c]) {
      if (str1[c] == '\0' || str2[c] == '\0')
         break;
      c++;
   }
 
   if (str1[c] == '\0' && str2[c] == '\0')
      return true;
   else
      return false;
}
