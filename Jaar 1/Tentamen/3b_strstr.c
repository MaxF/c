#include <stdio.h>
#include <stdbool.h>

char *strstr(char *haystack, char *needle);

int main(void)
{
	char haystack[] = "Weledelgestrenge bonkige boktor";
	char needle1[] = "bo";
	char needle2[] = "bok";
	char needle3[] = "Bok"; /* strings zijn hoofdlettergevoelig */
	char *substr;
	substr = strstr(haystack, needle1);
	printf("The substring is: %s\n", substr);
	substr = strstr(haystack, needle2);
	printf("The substring is: %s\n", substr);
	substr = strstr(haystack, needle3);
	printf("The substring is: %s\n", substr);
	printf("\n\n");
	return 0;
}

char *strstr(char *haystack, char *needle)
{
   int i = 0, j = 0, iminus = 0;
   char found = 0;
   char * firstocc = NULL;
   while((haystack[i] !='\0') || (needle[j] !='\0')){
		//Check if the first letter is found
		if(haystack[i] == needle[j]){
			//Start going through both strings and check if the string is the same till the '\0' character is reached
			while((haystack[i] !='\0') || (needle[j] !='\0')){
				i++;j++;iminus++;
				found=0;
				if(haystack[i] == needle[j])
				{
					found=1; //TRUE, this has to be the last statement
				}
				else{
					found=0;
					break;
				}
			}
		}
		else{
		//Else just increase haystack till it is at the same letter
		i++;
		}
   }
   if(found){
	   firstocc = haystack+i-iminus;
	   return firstocc;
	}
	else{
		return 0;
	}
}
