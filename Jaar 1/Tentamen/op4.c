#include <stdio.h>
typedef struct punt{
	double x;
	double y;
} punt;
punt bepaal_middelpunt(punt a, punt b){
	punt middelPunt; 
	//Bereken x
	middelPunt.x = (b.x + a.x)/2;
	middelPunt.y = (b.y + a.y)/2;
	return middelPunt;
}
int main(){
	punt a = {-2,-2};
	punt b = {3,5};
	punt c = {3.17, -7.61};
	punt d = {-5.809, 11.347};
	punt middel = bepaal_middelpunt( a, b);
	punt middel2 = bepaal_middelpunt(c,d);
	printf("Het middelpunt tussen A(-2,-2) en B (3,5) is: (%.1f,%.1f)\n",middel.x,middel.y);
	printf("Het middelpunt tussen A(3.17,-7.61) en B (-5.809,11.347) is: (%.4f,%.4f)",middel2.x,middel2.y);
}

