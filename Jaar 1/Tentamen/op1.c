#include <stdio.h>
typedef enum{
	speler0,
	speler1,
	speler2,
	speler3,
} speler;
unsigned short ontrafel_score(speler spelerindex, unsigned short transmissie);
int main(){
	unsigned short transmissie = 0xFA1C;
	unsigned short score = ontrafel_score(speler1, transmissie);
	printf("Transmissiedata: 0x%X\nScore speler %i: %i\n",transmissie, 1, score);
	score = ontrafel_score(speler3, transmissie);
	printf("Transmissiedata: 0x%X\nScore speler %i: %i\n",transmissie, 3, score);
	return 0;
}
unsigned short ontrafel_score(speler spelerindex, unsigned short transmissie){
	unsigned short score = 0;
	//Eerst de speler index checken
	if(spelerindex==0){
		//We moeten de rechtste bits hebben in een 16 bits waarde
		score = transmissie << 12;
		score = score >> 12;
	}
	if(spelerindex==1){
		score = transmissie << 8;
		score = score >> 12;
	}
	if(spelerindex==2){
		score = transmissie << 4;
		score = score >> 12;
	}
	if(spelerindex==3){
		score = transmissie >> 12;
	}
	return score;
}
