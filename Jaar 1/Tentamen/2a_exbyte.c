#include <stdio.h>

void *haal_byte_uit(void *brondata, size_t omvang, unsigned int doelbyte);

int main(void)
{
	unsigned long long data1 = 0xFFEEDDCCBBAA9988;
	char data2[] = "koeien zijn awesome!";
	unsigned char *byte;
	byte = (unsigned char *)haal_byte_uit((void *)&data1, sizeof(data1), 2);
	if (NULL != byte)
		printf("Byte is '%c' (0x%X)\n", *byte, *byte);
	byte = (unsigned char *)haal_byte_uit((void *)data2, sizeof(data2), 13);
	if (NULL!= byte)
		printf("Byte is '%c' (0x%X)\n", *byte, *byte);
	byte = (unsigned char *)haal_byte_uit((void *)data2, sizeof(data2), 113);
	if (NULL != byte)
		printf("Byte is '%c' (0x%X)\n", *byte, *byte);
	return 0;
}

void *haal_byte_uit(void *brondata, size_t omvang, unsigned int doelbyte){
	void * byte = (brondata+doelbyte);
	if (doelbyte>omvang){
		return NULL;
	}
	else{
		return byte;
	}
}
