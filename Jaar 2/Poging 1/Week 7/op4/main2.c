#include <stdio.h>

typedef enum {
	januari = 1,
	februari,
	maart,
	april,
	mei,
	juni,
	juli,
	augustus,
	september,
	oktober,
	november,
	december} MaandNummer;

typedef struct {
	MaandNummer maand_nummer;
	int aantal_dagen;
} Maand;

typedef struct {
	int dag;
	Maand maand;
} kalender_dag;

void initMaanden(Maand *maanden);
int kalenderdag_to_aantal(kalender_dag k);
void dag_to_string(kalender_dag *k, char *string_representatie);

int main()
{
	Maand maanden[12];
	char string1[10];
	char string2[10];
	//char *string1;
	//char *string2;
	initMaanden(maanden);
	kalender_dag dag1 = { 12, maanden[januari - 1] }; // 12 januari
	kalender_dag dag2 = { 17, maanden[oktober - 1] }; // 17 oktober
	dag_to_string(&dag1, string1);
	dag_to_string(&dag2, string2);
	printf("Het aantal dagen verschil tussen de data %s en %s is %d! (dag1: %d, dag2: %d)\n", string1, string2, (kalenderdag_to_aantal(dag2) - kalenderdag_to_aantal(dag1)), kalenderdag_to_aantal(dag1), kalenderdag_to_aantal(dag2));
	return 0;
}

void initMaanden(Maand *maanden)
{
	maanden[januari - 1] = (Maand){ januari, 31 };
	maanden[februari - 1] = (Maand){ februari, 28 };
	maanden[maart - 1] = (Maand){ maart, 31 };
	maanden[april - 1] = (Maand){ april, 30 };
	maanden[mei - 1] = (Maand){ mei, 31 };
	maanden[juni - 1] = (Maand){ juni, 30 };
	maanden[juli - 1] = (Maand){ juli, 31 };
	maanden[augustus - 1] = (Maand){ augustus, 31 };
	maanden[september - 1] = (Maand){ september, 30 };
	maanden[oktober - 1] = (Maand){ oktober, 31 };
	maanden[november - 1] = (Maand){ november, 30 };
	maanden[december - 1] = (Maand){ december, 31 };
}

int kalenderdag_to_aantal(kalender_dag k)
{
	Maand maanden[12];
	initMaanden(maanden);
	int count = 0;
	for(int i = 0; i < (int)k.maand.maand_nummer; i++)
	{
		for(int j = 0; j < maanden[i].aantal_dagen; j++)
		{
			if(i == (int)k.maand.maand_nummer - 1 && j == k.dag - 1)
			{
				break;
			}
			count++;
		}
	}
	return count + 1;
}

void dag_to_string(kalender_dag *k, char *string_representatie)
{
	sprintf(string_representatie, "%d-%d", k->dag, k->maand.maand_nummer);
}
