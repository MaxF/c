#include <stdio.h>

typedef enum { nul = 0, een } bitje;

bitje bepaal_derde_bit(int number);

int main()
{
	int numb;
	while(numb != 0)
	{
		printf("Geef een getal (0 = stoppen): ");
		scanf("%d", &numb);
		if(numb != 0)
			printf("Het derde bit van rechts van getal %d = %d\n\n", numb, bepaal_derde_bit(numb));
	}
	return 0;
}

bitje bepaal_derde_bit(int number)
{
	number = number >> 2;
	return number & 1;
}
