#include <stdio.h>

void copy_cstring(char *source, char *destination);
int get_length(char *string);

int main()
{
	char origineel[50];
	char kopie[50];
	printf("Geef een string om te kopiëren: \n");
	scanf("%s", origineel);
	copy_cstring(origineel, kopie);
	printf("\n\'kopie\' bevat de volgende string: %s\n", kopie);
	return 0;
}

void copy_cstring(char *source, char *destination)
{
	int length = get_length(source);
	for(int i = 0; i < length; i++)
	{
		//destination[i] = source[i];
		*(destination + i) = *(source + i); // -> is hetzelfde als bovenstaand
	}
}

int get_length(char *string)
{
	int i;
	for(i = 0; *string != '\0'; i++, string++);
	return i;
}
