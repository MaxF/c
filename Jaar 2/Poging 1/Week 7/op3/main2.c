#include <stdio.h>
#include <stdlib.h>

int strlen(char *s);
char *create_concatenated_cstring(char *string1, char *string2);
void destroy_cstring(char **string);

int main()
{
	char *string1 = "Common sense is genius ";
	char *string2 = "dressed in it's working clothes.";
	char *together = create_concatenated_cstring(string1, string2);
	printf("Aan elkaar geplakt vormen de strings de volgende " \
		"quote:\n\n\"%s\"\n\n", together);
	destroy_cstring(&together);
	if(together == NULL)
		printf("De string is inderdaad vernietigd\n");
	return 0;
}

int strlen(char *s)
{
	int i;
	for(i = 0; *s != '\0'; i++, s++);
	return i;
}

char *create_concatenated_cstring(char *string1, char *string2)
{
	int count = 0;
	int length1 = strlen(string1);
	int length2 = strlen(string2);
	int length = length1 + length2;
	char *concatenated_cstring = (char*)malloc(length);
	for(int i = 0; i < length1; i++)
	{
		*(concatenated_cstring + count) = string1[i];
		count++;
	}
	for(int i = 0; i < length2; i++)
	{
		*(concatenated_cstring + count) = string2[i];
		count++;
	}
	return concatenated_cstring;
}

void destroy_cstring(char **string)
{
	free(*string);
	*string = NULL;
}
