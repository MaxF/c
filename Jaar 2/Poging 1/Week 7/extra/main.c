#include <stdio.h>
#include <stdlib.h>

void string_heapcopy(const char *source, char *destination[]);
int stringLength(char *string);
void replace_first(char *string, char old, char new);
void string_destroy(char **string);

int main()
{
	const int BUFF_SIZE = 1024;
	char str[BUFF_SIZE];
	char *copy = NULL;
	
	printf("Enter a string: ");
	fgets(str, BUFF_SIZE, stdin);
	replace_first(str, '\n', '\0');
	str[BUFF_SIZE - 1] = '\0';
	
	string_heapcopy(str, &copy);
	
	printf("Contents of duplicate: %s\n", copy);
	printf("Address of inputstring: %p\n", str);
	printf("Address of duplicate: %p\n", copy);

	string_destroy(&copy);
	string_destroy(&copy);
	return 0;
}

void string_heapcopy(const char *source, char *destination[])
{
	int length = stringLength((char *)source);
	int size = length * sizeof(char*);
	*destination = (char *)malloc(size);
	for(int i = 0; i < length; i++)
	{
		//destination[0][i] = source[i];
		*(*destination + i) = source[i];
	}
}

int stringLength(char *string)
{
	int i;
	for(i = 0; *(string + i) != '\0'; i++);
	return i;
}

void replace_first(char *string, char old, char new)
{
	for(int i = 0; *(string + i) != '\0'; i++)
	{
		if(*(string + i) == old)
		{
			*(string + i) = new;
			break;
		}
	}
}

void string_destroy(char **string)
{
	if(*string == NULL)
	{
		printf("This string was already destroyed!\n");
		return;
	}
	free(*string);
	*string = NULL;
}
