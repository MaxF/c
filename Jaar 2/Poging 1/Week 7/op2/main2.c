#include <stdio.h>
#include <stdlib.h>

typedef struct Item {
	int *numb, *count;
} Item;
typedef struct _listitem {
	Item *data;
	struct _listitem *next;
} Listitem;

typedef struct {
	Listitem *head, *tail;
} List;
typedef enum { false = 0, true } bool;

void list_init(List *theList);
void list_add_to_tail(Item data, List *theList);
void list_print(List *theList);
void list_free(List *theList);
void countNumbers(int *getallen, int size);
void fillList(List *theList, int *getallen, int size);
bool numberExists(List *theList, int getal);
void quickSortList(Listitem *current, Listitem *next);

int main()
{
	int getallen[] = { 1, 5, 4, 7, 1, 4, 1, 7, 5, 5, 3, 1 };
	int *richtgetal = &getallen[0];
	countNumbers(getallen, sizeof(getallen)/sizeof(int));
	return 0;
}

void countNumbers(int *getallen, int size)
{
	int *last = getallen + size;
	printf("last: %d\n", *last);
	int *array = getallen;
	List myList;
	list_init(&myList);
	while(getallen != last)
	{
		int count = 0;
		if(!numberExists(&myList, *getallen))
		{
			Item item;
			item.numb = getallen;
			printf("getallen: %d\n", *getallen);
			for(int j = 0; j < size; j++)
			{
				if(*getallen == *(array + j))
					count++;
			}
			item.count = &count;
			printf("Add: numb: %d, count: %d\n", *item.numb, *item.count);
			list_add_to_tail(item, &myList);
		}
		getallen++;
	}
	//int number = 100, number2 = 1043;
	//Item item = { &number, &number2 };
	//list_add_to_tail(item, &myList);
	list_print(&myList);
	list_free(&myList);
}

void list_init(List *theList)
{
	theList->head = NULL;
	theList->tail = NULL;
}

void list_add_to_tail(Item data, List *theList)
{
	Listitem *item = (Listitem*)malloc(sizeof(Listitem));
	item->data = &data;
	item->next = NULL;
	if(theList->head == NULL)
	{
		theList->head = item;
		theList->tail = item;
	}
	else
	{
		theList->tail->next = item;
		theList->tail = item;
	}
	printf("%d, %d\n", *data.numb, *data.count);
}

void list_print(List *theList)
{
	Listitem *point = theList->head;
	while(point != NULL)
	{
		printf("Het aantal voorkomens van %d is: %d, (%p)\n", *point->data->numb, *point->data->count, point);
		point = point->next;
	}
}

void list_free(List *theList)
{
	Listitem *current, *next;
	current = theList->head;
	while(current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
	list_init(theList);
}

/*void fillList(List *theList, int *getallen, int size)
{
	for(int i = 0; i < size; i++)
	{
		if(!numberExists(theList, getallen[i]))
		{
			list_add_to_tail(getallen[i], theList);
		}
	}
}*/

bool numberExists(List *theList, int getal)
{
	Listitem *point = theList->head;
	while(point != NULL)
	{
		if(point->data->numb == getal)
		{
			return true;
		}
		else
			point = point->next;
	}
	return false;
}

void quickSortList(Listitem *current, Listitem *next)
{
	while(next != NULL)
	{
		//printf("current: %d, next: %d\n", (int*)current->data, (int*)next->data);
		if((int*)current->data > (int*)next->data)
		{
			int temp = current->data;
			current->data = next->data;
			next->data = temp;
		}
		current = next;
		next = next->next;
		quickSortList(current, next);
	}
}
