#include <stdio.h>

#define LENGTH 5

void addNumber(int numbers[], int number);
void shiftArray(int numbers[]);
void printArray(int numbers[]);

int main()
{
	int number = -1;
	int numbers[] = { 0, 0, 0, 0, 0 };
	while(number != 0)
	{
		printf("Geef het (volgende) getal <0 = stoppen>: ");
		scanf("%d", &number);
		if(number != 0) addNumber(numbers, number);
	}
	printf("\n\n");
	printArray(numbers);
	return 0;
}

void addNumber(int numbers[], int number)
{
	shiftArray(numbers);
	numbers[0] = number;
}

void shiftArray(int numbers[])
{
	for(int i = LENGTH - 1; i > 0; i--)
	{
		numbers[i] = numbers[i - 1];
	}
}

void printArray(int numbers[])
{
	for(int i = LENGTH; i > 0; i--)
	{
		printf("Getal %d is : %d\n", LENGTH - i + 1, numbers[i - 1]);
	}
}
