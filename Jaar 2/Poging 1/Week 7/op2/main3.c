#include <stdio.h>

typedef struct {
	int number, count;
} Item;
typedef enum { false = 0, true } bool;

int getUnique(int *numbers, int size);
void fillArray(int *numbers, int size, int number);
void quickSort(int *numbers, int size);
void swap(int *number1, int *number2);
void countOccurrences(int *number, int *input, int *numbers, int *count, int unique, int size);
bool checkExists(int number, int *numbers, int size);
void addToArray(int value, int *numbers, int size, int unique);
void printResults(int *numbers, int *count, int size, int unique);

int main()
{
	int getallen[] = { 1, 5, 4, 7, 1, 4, 1, 7, 5, 5, 3, 1 };
	int size = sizeof(getallen)/sizeof(int);
	quickSort(getallen, size);
	int unique = getUnique(getallen, size);
	int numbers[size];
	int count[size];
	
	fillArray(numbers, size, unique);
	fillArray(count, size, unique);

	for(int i = 0; i < size; i++)
	{
		int *point = &getallen[i];
		countOccurrences(point, getallen, numbers, count, unique, size);
	}
	printResults(numbers, count, size, unique);
	return 0;
}

int getUnique(int *numbers, int size)
{
	bool complete = false;
	int unique = 0;
	while(!complete)
	{
		for(int i = 0; i < size; i++)
		{
			if(*(numbers + i) == unique)
			{
				unique++;
				break;
			}
			if(i == size - 1)
				complete = !complete;
		}
	}
	return unique;
}

void fillArray(int *numbers, int size, int number)
{
	for(int i = 0; i < size; i++)
	{
		numbers[i] = number;
	}
}

void quickSort(int *numbers, int size)
{
	for(int i = 0; i < size - 1; i++)
	{
		if(*(numbers + i) > *(numbers + i + 1))
		{
			swap(numbers + i, numbers + i + 1);
			quickSort(numbers, size);
		}
	}
}

void swap(int *number1, int *number2)
{
	int temp = *number1;
	*number1 = *number2;
	*number2 = temp;
}

void countOccurrences(int *number, int *input, int *numbers, int *count, int unique, int size)
{
	if(!checkExists(*number, numbers, size))
	{	
		int counter = 0;
		for(int i = 0; i < size; i++)
		{
			if(*number == input[i])
				counter++;
		}
		addToArray(*number, numbers, size, unique);
		addToArray(counter, count, size, unique);
	}
}

bool checkExists(int number, int *numbers, int size)
{
	for(int i = 0; i < size; i++)
	{
		if(numbers[i] == number)
			return true;
	}
	return false;
}

void addToArray(int value, int *numbers, int size, int unique)
{
	for(int i = 0; i < size; i++)
	{
		if(numbers[i] == unique)
		{
			numbers[i] = value;
			break;
		}
	}
}

void printResults(int *numbers, int *count, int size, int unique)
{
	for(int i = 0; i < size && numbers[i] != unique; i++)
	{
		printf("Het aantal voorkomens van %d is: %d\n", numbers[i], count[i]);
	}
}
