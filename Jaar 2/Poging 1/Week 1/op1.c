#include <stdio.h>
#include <stdbool.h>

void displayBiggest(int numbers[], int size);
bool exists(int numbers[], int size, int value);
void printArray(int numbers[], int size);
int main()
{
	int numbers [6] = { 5, 2, 3, 8, 9, 12 };
	printArray(numbers, 6);
	displayBiggest(numbers, sizeof(numbers)/sizeof(*numbers));
	return 0;
}

void displayBiggest(int numbers[], int size)
{
	int biggest[3] = { 0, 0, 0 };
	int biggestSize = sizeof(biggest)/sizeof(*biggest);
	for(int i = 0; i < biggestSize; i++)
	{
		for(int j = 0; j < size; j++)
		{
			if(numbers[j] > biggest[i])
			{
				if(!exists(biggest, biggestSize, numbers[j])) 
				{
					biggest[i] = numbers[j];
				}
			}
		}
	}
	printf("De 3 grootste getallen zijn: %d, %d, %d.\n", biggest[0], biggest[1], biggest[2]);
}

bool exists(int numbers[], int size, int value)
{
	for(int i = 0; i < size; i++)
	{
		if(numbers[i] == value) return true;
	}
	return false;
}
void printArray(int numbers[], int size)
{
	for(int i = 0; i < size; i++)
	{
		printf("%d, ", numbers[i]);
	}
	printf("\n");
}
