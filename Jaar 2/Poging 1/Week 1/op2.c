#include <stdio.h>

void printNumberArray(int numbers[], int size);
void switchArray(int numbers[], int size);

int main ()
{
	int numbers [5] = { 5, 2, 7, 2, 23 };
	int numbersSize = sizeof(numbers)/sizeof(*numbers);
	printNumberArray(numbers, numbersSize);
	switchArray(numbers, numbersSize);
	return 0;
}

void printNumberArray(int numbers[], int size)
{
	printf("{ ");
	for(int i = 0; i < size; i++)
	{
		if(i == size - 1) printf("%d ", numbers[i]);
		else printf("%d, ", numbers[i]);
	}
	printf(" }\n");
}

void switchArray(int numbers[], int size)
{
	int switchedNumbers[size];
	for(int i = 0; i < size; i++)
	{
		switchedNumbers[i] = numbers[size - i - 1];
	}
	printNumberArray(switchedNumbers, size);
} 
