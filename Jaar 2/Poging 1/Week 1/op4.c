#include <stdio.h>

void sortChars(char chars[], int size);
void printArray(char chars[], int size);

int main()
{
	char chars[5] = { 'e', 'a', 'd', 'x', 'b' };
	int size = sizeof(chars)/sizeof(*chars);
	printArray(chars, size);
	sortChars(chars, size);
	return 0;
}

void sortChars(char chars[], int size)
{
	for(int i = 0; i < size; i++)
	{
		for(int j = i; j < size; j++)
		{
			if(chars[i] > chars[j])
			{
				char temp = chars[i];
				chars[i] = chars[j];
				chars[j] = temp;
			}
		}
	}
	printArray(chars, size);
}

void printArray(char chars[], int size)
{
	for(int i = 0; i < size; i++)
	{
		if(i == size -1) printf("%c\n", chars[i]);
		else printf("%c, ", chars[i]);
	}
}
