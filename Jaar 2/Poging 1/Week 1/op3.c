#include <stdio.h>

int findNumber(int numbers[], int size, int value);

int main()
{
	int numbers[5] = { 4, 36, 2, 3, 8 };
	int index = findNumber(numbers, sizeof(numbers)/sizeof(*numbers), 8);
	if(index != -1) printf("Index: %d\n", index);
	else printf("Gezochte getal komt niet voor in de array\n");
	return 0;
}

int findNumber(int numbers[], int size, int value)
{
	for(int i = 0; i < size; i++)
	{
		if(numbers[i] == value) return i;
	}
	return -1;
}
