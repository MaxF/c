#include <stdio.h>

typedef struct {
	char voornaam[50];
	char achternaam[50];
	int leeftijd;
	double uurtarief;
	int gewerkte_uren;
} opdrachtnemer;

void rapporteerBeloningen(opdrachtnemer opdrNemers[], int size);

int main()
{
	opdrachtnemer opdrNemers[] = {
		{"Koos", "Korswagen", 32, 135.75, 89},
		{"Bolus", "Bonkpok", 44, 162.25, 24},
		{"Bonkige", "Harry", 26, 89.45, 46}
	};
	int s = sizeof(opdrNemers)/sizeof(opdrachtnemer);
	rapporteerBeloningen(opdrNemers, s);
	return 0;
}

void rapporteerBeloningen(opdrachtnemer opdrNemers[], int size)
{
	for(int i = 0; i < size; i++)
	{
		printf("%s %s \t(%d) heeft recht op %10.2f euro\n", opdrNemers[i].voornaam, opdrNemers[i].achternaam, opdrNemers[i].leeftijd, opdrNemers[i].uurtarief*opdrNemers[i].gewerkte_uren);
	}
}
