#include <stdio.h>
#include <stdbool.h>
#include "lib.h"

int main()
{
	int number1, number2, binary1[32], binary2[32], xor[32];
	printf("Voer een getal in: ");
	scanf("%d", &number1);
	printf("Voer nog een getal in: ");
	scanf("%d", &number2);
	printf("\n");
	makeBinary(number1, binary1, 32);
	makeBinary(number2, binary2, 32);
	XOR(binary1, binary2, xor, 32);
	printBinary(binary1, number1, 32);
	printBinary(binary2, number2, 32);
	printf("\t\t\t\t ---------------------------------------\n");
	printXOR(xor, 32);
	return 0;
}
