#ifndef LIB_H
#define LIB_H

void makeBinary(int number, int binary[], int binarySize);
void XOR(int binary1[], int binary2[], int XOR[], int binarySize);
void printXOR(int XOR[], int size);
void printBinary(int binary[], int number, int size);
bool isNegative(int number);
bool isPositive(int number);
bool isZero(int number);

#endif
