#include <stdio.h>
#include <stdbool.h>
#include "lib.h"

void makeBinary(int number, int binary[], int binarySize)
{
	int result = number;
	for(int i = 0; i < binarySize; i++)
	{
		binary[i] = 0;
	}
	for(int i = 0; result != 0; i++)
	{
		int temp =  result%2;
		if(isNegative(temp))
			binary[i] = -temp;
		else if(isPositive(temp))
			binary[i] = temp;
		result = result/2;
	}
	if(isNegative(number) && !isZero(number))
	{
		for(int i = 0; i < binarySize; i++)
		{
			if(binary[i] == 1)
			{
				for(int j = i + 1; j < binarySize; j++)
				{
					if(binary[j] == 1) binary[j] = 0;
					else if(binary[j] == 0) binary[j] = 1;
				}
				break;
			}
		}
	}
}

void XOR(int binary1[], int binary2[], int XOR[], int binarySize)
{
	for(int i = 0; i < binarySize; i++)
	{
		XOR[i] = binary1[i] ^ binary2[i];
	}
}

void printXOR(int XOR[], int size)
{
	int count = 0;
	printf("XOR van beide getallen:\t\t");
        for(int i = size; i > 0; i--)
        {
		if(XOR[i] == 0) count++;
                if(i%4 == 0)
                        printf(" %d", XOR[i-1]);
                else
                        printf("%d", XOR[i-1]);
        }
        printf("\n\nBeide getallen komen op %d bitposities overeen!\n", count);

}
void printBinary(int binary[], int number, int size)
{
	printf("Het getal %10d is binair: ", number);
	for(int i = size; i > 0; i--)
	{
		if(i%4 == 0)
			printf(" %d", binary[i-1]);
		else
			printf("%d", binary[i-1]);
	}
	printf("\n");
}

bool isPositive(int number)
{
	return number > 0;
}

bool isNegative(int number)
{
	return number < 0;
}

bool isZero(int number)
{
	return number == 0;
}
