#ifndef LIB_H
#define LIB_H

void makeBinary(int number);
void printBinary(int binary[], int number, int size);
bool isNegative(int number);
bool isPositive(int number);
bool isZero(int number);

#endif
