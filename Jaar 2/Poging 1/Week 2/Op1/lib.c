#include <stdio.h>
#include <stdbool.h>
#include "lib.h"

void makeBinary(int number)
{
	int binary[32], result = number, binarySize = sizeof(binary)/sizeof(*binary);
	for(int i = 0; i < binarySize; i++)
	{
		binary[i] = 0;
	}
	for(int i = 0; result != 0; i++)
	{
		int temp =  result%2;
		if(isNegative(temp))
			binary[i] = -temp;
		else if(isPositive(temp))
			binary[i] = temp;
		result = result/2;
	}
	if(isNegative(number) && !isZero(number))
	{
		for(int i = 0; i < binarySize; i++)
		{
			if(binary[i] == 1)
			{
				for(int j = i + 1; j < binarySize; j++)
				{
					if(binary[j] == 1) binary[j] = 0;
					else if(binary[j] == 0) binary[j] = 1;
				}
				break;
			}
		}
	}
	printBinary(binary, number, binarySize);
}

void printBinary(int binary[], int number, int size)
{
	printf("Het getal %d is binair: ", number);
	for(int i = size; i > 0; i--)
	{
		if(i%4 == 0)
			printf(" %d", binary[i-1]);
		else
			printf("%d", binary[i-1]);
	}
	printf("\n");
}

bool isPositive(int number)
{
	return number > 0;
}

bool isNegative(int number)
{
	return number < 0;
}

bool isZero(int number)
{
	return number == 0;
}
