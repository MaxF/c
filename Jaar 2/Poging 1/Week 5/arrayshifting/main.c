#include <stdio.h>

void tidy_significands(int *data_array, int size);
void print_array(int *data_array, int size, int array_number, char* IO);

int main()
{
	char* Input = "input";
	char* Output = "output";
	int numbers[8] = { 0, 0, 0, 0, 0, 743, 2489, 62 };
	int numbers2[5] = { 0, 0, 0, 0, 0 };
	int numbers3[7] = { 0, 0, 15, 17, 19, 21, 23 };
	int numbers4[9] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	
	print_array(numbers, 8, 1, Input);
	tidy_significands(numbers, 8);
	print_array(numbers, 8, 1, Output);
	
	print_array(numbers2, 5, 2, Input);
	tidy_significands(numbers2, 5);
	print_array(numbers2, 5, 2, Output);

	print_array(numbers3, 7, 3, Input);
	tidy_significands(numbers3, 7);
	print_array(numbers3, 7, 3, Output);
	
	print_array(numbers4, 9, 4, Input);
	tidy_significands(numbers4, 9);
	print_array(numbers4, 9, 4, Output);

	return 0;
}

void tidy_significands(int *data_array, int size)
{
	if(*(data_array) != 0) return;
	int index = 0;
	for(int i = 0; i < size; i++)
	{
		if(*(data_array + i) != 0)
		{
			*(data_array + index) = *(data_array + i);
			*(data_array + i) = 0;
			index++;
		}
	}
}

void print_array(int *data_array, int size, int array_number, char* IO)
{
	printf("Array %d %6s: {", array_number, IO);
	for(int i = 0; i < size; i++)
	{
		if(i == size - 1)
			printf("%5d", *(data_array + i));
		else
			printf("%5d,", *(data_array + i));
	}
	if(*(IO) == 'i')
		printf(" }\n");
	else
		printf(" }\n\n");
}
