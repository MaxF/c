#include <stdio.h>
#include <math.h>

#define MIN_X -40
#define MIN_Y -12
#define MAX_X 40
#define MAX_Y 12

typedef struct { 
	int x; 
	int y; 
} point;
typedef struct { 
	point point1; 
	point point2; 
} line;

void plot_line(line *k);
int MIN(int a, int b);
int MAX(int a, int b);
double get_a(line *k);
double get_b(line *k, double a);
double get_y(double a, int x, double b);

int main()
{
	//point point1 = { -25, -7 };
	//point point2 = { 30, 5 };
	//point point1 = { -233, 0 };
	//point point2 = { -45, 80 };
	//point point1 = { -200, -200 };
	//point point2 = { 200, 200 };
	point point1 = { -30, 20 };
	point point2 = { 44, -20 };
	line Line = { point1, point2 };
	plot_line(&Line);
	return 0;
}

void plot_line(line *k)
{
	double a = get_a(k);
	double b = get_b(k, a);
	printf("a: %f, b: %f\n", a, b);
	for(int i = MAX_Y; i != MIN_Y; i--)
	{
		if(i != 0)
		{
			for(int j = MIN_X; j != MAX_X; j++)
			{
				if(j <= MAX(k->point1.x, k->point2.x) && j >= MIN(k->point1.x, k->point2.x))
				{				
					double y = get_y(a, j, b);
					if(i == (int)round(y))
						printf("*");
					else if(j == 0)
						printf("|");
					else
						printf(" ");
				}
				else
				{
					if(j == 0)
						printf("|");
					else
						printf(" ");
				}
			}
			printf("\n");
		}
		else
		{
			for(int j = MIN_X; j < MAX_X; j++)
			{
				if(j <= MAX(k->point1.x, k->point2.x) && j >= MIN(k->point1.x, k->point2.x))
				{
					double y = get_y(a, j, b);
					if(i == (int)round(y))
						printf("*");				
					else if(j == 0) 
						printf("+");
					else
						printf("-");
				}
				else
				{
					if(j == 0)
                                                printf("+");
                                        else
                                                printf("-");
				}
			}
			printf("\n");
		}
	}
}

int MIN(int a, int b)
{
	if(a < b) return a;
	else return b;
}

int MAX(int a, int b)
{
	if(a > b) return a;
	else return b;
}

double get_a(line *k)
{
	int delta_y = k->point1.y - k->point2.y;
	int delta_x = k->point1.x - k->point2.x;
        return (double)delta_y / (double)delta_x;
}

double get_b(line *k, double a)
{
	double y = (double)a * (double)k->point1.x;
	return (double)k->point1.y - y;
}

double get_y(double a, int x, double b)
{
	double y = (a * (double)x) + b;
	return y;
}
