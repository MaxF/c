#include <stdio.h>

#define MIN_X -40
#define MIN_Y -12
#define MAX_X 40
#define MAX_Y 12

void plot_grid();

int main()
{
	plot_grid();
	return 0;
}

void plot_grid()
{
	for(int i = MAX_Y; i != MIN_Y; i--)
	{
		if(i != 0)
		{
			for(int j = MIN_X; j != 0; j++)
			{
				printf(" ");
			}
			printf("|\n");
		}
		else
		{
			for(int j = MIN_X; j < MAX_X; j++)
			{
				if(j == 0) 
				{
					printf("+");
				}
				else
				{
					printf("-");
				}
			}
			printf("\n");
		}
	}
}
