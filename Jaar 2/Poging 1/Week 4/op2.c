#include <stdio.h>
#include <stdlib.h> // for malloc()

typedef struct _listitem {
    void *data;
    struct _listitem *next;
} Listitem;

typedef struct {
    Listitem *head, *tail;
} List;

void list_init(List *theList);
void list_add_to_tail(void *data, List *theList);
void list_print(List *theList);
void list_free(List *theList);

int main()
{
	char *a = "Rome", *b = "Seattle", *c = "Toronto", *d = "Zurich";
    	List myList;
    	list_init(&myList);
    	list_add_to_tail(a, &myList);
    	list_add_to_tail(b, &myList);
   	list_add_to_tail(c, &myList);
   	list_add_to_tail(d, &myList);
   	list_print(&myList);
   	list_free(&myList);
   	return 0;
}

void list_init(List *theList)
{
    	theList->head = NULL; // points to nothing
    	theList->tail = NULL; // points to nothing
}

void list_add_to_tail(void *data, List *theList)
{
    	// 1. Maak een nieuwe Listitem aan met malloc()
	Listitem *item = (Listitem*)malloc(sizeof(Listitem));
    	// 2. Zet ‘data’ in de Listitem en verwijs met next naar NULL (wordt immers de nwe tail)
	item -> data = data;
	item -> next = NULL;
    	// 3. Als de lijst leeg is:
    	//      > Laat zowel de head- als de tail-pointer naar het nieuwe item wijzen
    	//    Als de lijst niet leeg is:
    	//      > De ‘next’-pointer v.h. huidige laatste item moet naar het nieuwe laatste
    	//        item gaan wijzen
    	//      > De tail-pointer moet vervolgens ook naar het nieuwe laatste item gaan wijzen
	if(theList -> head == NULL)
	{
		theList -> head = item;
		theList -> tail = item;
	}
	else
	{
		theList -> tail -> next = item;
		theList -> tail = item;
	}
}


void list_print(List *theList)
{
    	// Als de lijst niet leeg is:
    	//   > Laat een ‘langsloop’-variabele wijzen naar de head van de list
    	//   > Zolang de langsloop-variabele niet NULL is
    	//     - Print het huidige data-item
    	//     - Laat de langsloop-variabele wijzen naar het item waar de ‘next’-pointer
    	//       van het huidige item naar wijst
	if(theList -> head != NULL && theList -> tail != NULL)
	{
		Listitem *point = theList -> head;
		while(point != NULL)
		{
			printf("List item with c-string data: %s\n", point -> data);
			point = point -> next;
		}
    	}
	else
	{
		printf("De lijst is leeg!");
	}
	// Als de lijst wel leeg is:
    	//   > Print “de lijst is leeg!”
}

void list_free(List *theList)
{
    	Listitem *current, *next;
    	current = theList->head;
    	while (current != NULL)
    	{
    	    next = current->next;
    	    free(current);
    	    current = next;
    	}
    	list_init(theList); // list is empty, so head and tail should point to NULL
}	
