#include <stdio.h>

#define MAX_STR_LEN 100

void reverse(char *cstr);
int getLength(char *cstr);

int main()
{
	char a[MAX_STR_LEN];
	printf("Geef een string (max %d tekens): ", MAX_STR_LEN);
	scanf("%s", a);
	reverse(a);
	printf("%s\n", a);
	return 0;
}

void reverse(char *cstr)
{
	int j = getLength(cstr);
	char temp;
	for(int i = 0; i < j; i++, j--)
	{
		temp = cstr[i];
		cstr[i] = cstr[j-1];
		cstr[j-1] = temp;
	}
}

int getLength(char *cstr)
{
	int i = 0;
	while(cstr[i] != '\0') i++;
	return i;
}
