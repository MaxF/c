#include <stdio.h>
#include <stdlib.h>

#define ARR_SIZE 12

int max_value(int *some_array, int size);

int main()
{
	int i, max, the_array[ARR_SIZE];
	for(i = 0; i < ARR_SIZE; i++) *(the_array +i) = rand() % 1000;
	for(i = 0; i < ARR_SIZE; i++) printf("%6d, ", *(the_array + i));
	printf("\n");
	max = max_value(the_array, ARR_SIZE);
	printf("Max = %6d\n", max);
	return 0;
}

int max_value(int *some_array, int size)
{
	int biggest = 0;
	for(int i = 0; i < size; i++)
	{
		if(*(some_array + i) > biggest) biggest = *(some_array + i);
	}
	return biggest;
}
