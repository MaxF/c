#include <stdio.h>

#define MAX_LENGTH 100

typedef enum { false, true } bool;

bool is_palindrome(char *cstr);
void removeEnterKey(char *cstr);
int getLength(char *cstr);

int main()
{
	char a[MAX_LENGTH];
	printf("Voer een woord in: ");
	fgets(a, MAX_LENGTH, stdin);
	removeEnterKey(a);
	if(is_palindrome(a)) printf("'%s' is een palindroom", a);
	else printf("'%s' is geen palindroom", a);
	printf("\n");
	return 0;
}

bool is_palindrome(char *cstr)
{
	int size = getLength(cstr);
	for(int i = 0, j = size - 1; i != j || i > j; i++, j--)
	{
		if(*(cstr + i) != *(cstr + j)) return false;
	}
	return true;
}

void removeEnterKey(char *cstr)
{
	cstr[getLength(cstr)-1] = '\0';
}

int getLength(char *cstr)
{
	int count = 0;
	for(int i = 0; cstr[i] != '\0'; i++)
	{
		count++;
	}
	return count;
}
