#include <stdio.h>

void deel_geheel(unsigned int teller, unsigned int noemer, unsigned int *quot, unsigned int *rem);

int main()
{
	unsigned int teller, noemer;
	unsigned int quot, rem;
	printf("Geef de teller: ");
	scanf("%d", &teller);
	printf("Geef de noemer: ");
	scanf("%d", &noemer);
	if(noemer != 0)
	{
		deel_geheel(teller, noemer, &quot, &rem);
		printf("%d / %d = %d rest %d\n", teller, noemer, quot, rem);
	}
	else printf("Delen door 0 is flauwekul!\n");
	return 0;
}

void deel_geheel(unsigned int teller, unsigned int noemer, unsigned int *quot, unsigned int *rem)
{
	*quot = teller/noemer;
	*rem = teller%noemer;
}
