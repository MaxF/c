#include <stdio.h>


void printbyte( char d ) {
	unsigned int c = d & 0xff;
	if ((c >= 0x20) && ( c < 0x80)) {
		printf( "%02x:%c", c, c);
	} else {
		printf( "%02x:?", c);
	}	
}


/*********************************************************
   void dumpstack() 
   dump aantal bytes van de stack, startend vanaf startaddr
   oplopende adressen.
*********************************************************/
void dumpstack( char * startaddr, int aantal) {
	char * ptr = startaddr;
	int byte;
	int i;
	for (i = 1; i<= aantal; i++) {
		byte = *ptr & 0xff;
		printf( "%p: ", ptr );
		printbyte(byte);
		printf("\n");
		ptr++;
	}
}

void democall(char parArray[] ) {
	 printf("\n\ndemocall: adres van parArray parameter is %p\n", &parArray);
	 printf("parArray[1] = %c\n", parArray[1]);
	 printf("Merk op dat het adres van het oorspronkelijke array op stack is gezet:\n" );
	 printf("De volgende lijst begint met de 4 bytes van het adres van chararray\n");
	 dumpstack( (char *) &parArray, 10 );
	 parArray[1] = 'X';
}

/*********************************************************
   int main()
*********************************************************/
int main() {
	char chararray[] = "adriaan";
	printf("adres van chararray is %p\n", &chararray);
	printf("chararray[1] = %c\n", chararray[1]);

	democall(chararray);

	printf(" Na aanroep van democall: chararray[1] = %c\n", chararray[1]);
	printf(" De aanroep van democall heeft dus een wijziging in het array veroorzaakt.\n");
	return 0;
}
