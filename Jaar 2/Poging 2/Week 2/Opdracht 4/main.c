#include <stdio.h>

typedef struct{
	char name[50];
	char familyName[50];
	int age;
	double salary;
	int hours;
} contractor;

void printContractors(contractor *contractors, int size);

int main()
{
	contractor contractors[] = {
		{ "Koos", "Korswagen", 32, 135.75, 89 },
		{ "Bolus", "Bonkpok", 44, 162.25, 24 },
		{ "Bonkige", "Harry", 26, 89.45, 46 }
	};
	int size = sizeof(contractors)/sizeof(contractor);
	printContractors(contractors, size);
	return 0;
}

void printContractors(contractor *contractors, int size)
{
	for(int i = 0; i < size; i++)
	{
		double salary = (double)contractors[i].hours * contractors[i].salary;
		printf("%-7s %-9s (%d jaar) heeft recht op %8.2f euro\n", contractors[i].name, contractors[i].familyName, contractors[i].age, salary);
	}
}
