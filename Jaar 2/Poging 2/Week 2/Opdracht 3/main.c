#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define checkEqual(numb1, numb2) numb1==numb2

typedef enum {
	false = 0,
	true = 1 
	} bool;

void initTimer();
void generateNumber(int *numb);

int main()
{
	initTimer();
	int numb, numb2;
	generateNumber(&numb);
	generateNumber(&numb2);
	if(checkEqual(numb, numb2))
		printf("%d and %d are equal\n", numb, numb2);
	else
		printf("%d and %d are not equal\n", numb, numb2
		);
	return 0;
}

void initTimer()
{
	srand(time(NULL));
}

void generateNumber(int *numb)
{
	*numb = rand() % 6;
}
