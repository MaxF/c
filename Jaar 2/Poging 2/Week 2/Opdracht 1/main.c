#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef enum { low = 0, high = 1 } bit;

void initTimer();
void generateNumber(int *numb);
void decimalToBinary(bit *data, int size, int numb);
void printBinary(bit *data, int size, int numb);

int main()
{
	int numb;
	bit data[32];
	initTimer();
	generateNumber(&numb);
	int size = sizeof(data)/sizeof(bit);
	decimalToBinary(data, size, numb);
	printBinary(data, size, numb);
	return 0;
}

void initTimer()
{
	srand(time(NULL));
}

void generateNumber(int *numb)
{
	*numb = rand() % 1001 - 500;
}

void decimalToBinary(bit *data, int size, int numb)
{
	for(int i = size - 1; i >= 0; i--)
	{
		data[i] = numb & 1;
		numb = numb >> 1;
	}
}

void printBinary(bit *data, int size, int numb)
{
	printf("Het getal %10d is binair: ", numb);
	for(int i = 0; i < size; i++)
	{
		if((i + 1) % 4 == 0)
			printf("%d ", data[i]);
		else
			printf("%d", data[i]);
	}
}
