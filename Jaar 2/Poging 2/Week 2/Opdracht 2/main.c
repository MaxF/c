#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef enum { low = 0, high = 1 } bit;

void initTimer();
void generateNumber(int *numb);
void decimalToBinary(bit *data, int size, int numb);
void printBinary(bit *data, int size, int numb);
void XOR(bit *XORdata, bit *data, bit *data2, int XORsize, int size, int size2);
void printXOR(bit *XORdata, int XORsize);

int printCnt = 0;

int main()
{
	int numb, numb2;
	bit data[32], data2[32], XORdata[32];
	initTimer();
	generateNumber(&numb);
	generateNumber(&numb2);
	int size = sizeof(data)/sizeof(bit);
	int size2 = sizeof(data2)/sizeof(bit);
	int XORsize = sizeof(XORdata)/sizeof(bit);
	decimalToBinary(data, size, numb);
	decimalToBinary(data2, size2, numb2);
	XOR(XORdata, data, data2, XORsize, size, size2);
	printBinary(data, size, numb);
	printBinary(data2, size2, numb2);
	printXOR(XORdata, XORsize);
	return 0;
}

void initTimer()
{
	srand(time(NULL));
}

void generateNumber(int *numb)
{
	*numb = rand() % 10001 - 5000;
}

void decimalToBinary(bit *data, int size, int numb)
{
	for(int i = size - 1; i >= 0; i--)
	{
		data[i] = numb & 1;
		numb = numb >> 1;
	}
}

void printBinary(bit *data, int size, int numb)
{
	printCnt++;
	printf("Getal %d <%6d> is binair: ", printCnt, numb);
	for(int i = 0; i < size; i++)
	{
		if((i + 1) % 4 == 0)
			printf("%d ", data[i]);
		else
			printf("%d", data[i]);
	}
	printf("\n");
}

void XOR(bit *XORdata, bit *data, bit *data2, int XORsize, int size, int size2)
{
	for(int i = 0; i < XORsize && i < size && i < size2; i++)
	{
		XORdata[i] = data[i] ^ data2[i];
	}
}

void printXOR(bit *XORdata, int XORsize)
{
	printf("\t\t\t    ---------------------------------------\n");
	printf("XOR voor beide getallen\t  : ");
	for(int i = 0; i < XORsize; i++)
	{
		if(XORdata[i] == low)
		{
			if((i + 1) % 4 == 0)
				printf("%d ", low);
			else
				printf("%d", low);
		}
		else
		{
			if((i + 1) % 4 == 0)
				printf("  ");
			else
				printf(" ");
		}
	}
}
