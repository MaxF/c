#include <stdio.h>
#include <time.h>
#include <stdlib.h>

typedef enum{
	false = 0,
	true = 1
} bool;

void generateNumbers(int numbers[], int size);
void printArray(int *numbers, int size);
void printBiggest(int *numbers, int size, int amount);
bool exists(int *numbers, int size, int number);

int main()
{
	int numbers[10];
	int size = sizeof(numbers)/sizeof(int);
	generateNumbers(numbers, size);
	printArray(numbers, size);
	printBiggest(numbers, size, 3);
	return 0;
}

void generateNumbers(int numbers[], int size)
{
	srand(time(NULL));
	for(int i = 0; i < size; i++) numbers[i] = rand() % 101;
}

void printArray(int *numbers, int size)
{
	for(int i = 0; i < size; i++)
	{
		printf("numbers[%d] = %d\n", (i + 1), numbers[i]);
	}
	printf("\n");
}

void printBiggest(int *numbers, int size, int amount)
{
	int biggest[amount];
	for(int i = 0; i < amount; i++)
	{
		biggest[i] = numbers[0];
		for(int j = 0; j < size; j++)
		{
			if(biggest[i] < numbers[j] && !exists(biggest, amount, numbers[j]))
				biggest[i] = numbers[j];
		}
		printf("Biggest %d: %d\n", i + 1, biggest[i]);
	}
}

bool exists(int *numbers, int size, int number)
{
	for(int i = 0; i < size; i++)
	{
		if(numbers[i] == number) return true;
	}
	return false;
}
