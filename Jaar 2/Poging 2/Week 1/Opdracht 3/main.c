#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void initTime();
void generateNumber(int *number, int range);
void generateNumbers(int *numbers, int size);
void printArray(int *numbers, int size);
void findWantedIndex(int *numbers, int size, int wantedNumber);

int main()
{
	int wantedIndex;
	int numbers[10];
	int size = sizeof(numbers)/sizeof(int);
	initTime();
	generateNumbers(numbers, size);
	printArray(numbers, size);
	generateNumber(&wantedIndex, size);
	findWantedIndex(numbers, size, numbers[wantedIndex]);
	return 0;
}

void initTime()
{
	srand(time(NULL));
}

void generateNumber(int *number, int range)
{
	*number = rand() % range;
}

void generateNumbers(int *numbers, int size)
{
	for(int i = 0; i < size; i++)
	{
		numbers[i] = rand() % 101; // range: 0 t/m 100
	}
}

void printArray(int *numbers, int size)
{
	for(int i = 0; i < size; i++)
	{
		printf("numbers[%d] = %d\n", i, numbers[i]);
	}
	printf("\n");
}

void findWantedIndex(int *numbers, int size, int wantedNumber)
{
	printf("Wanted number: %d\n", wantedNumber);
	for(int i = 0; i < size; i++)
	{
		if(numbers[i] == wantedNumber)
			printf("Wanted index: %d\n", i);
	}
}
