#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void generateNumbers(int *numbers, int size);
void printArray(int *numbers, int size);
void swapArray(int *numbers, int size);
int main()
{
	int numbers[5];
	int size = sizeof(numbers)/sizeof(int);
	generateNumbers(numbers, size);
	printArray(numbers, size);
	swapArray(numbers, size);
	printArray(numbers, size);
	return 0;
}

void generateNumbers(int *numbers, int size)
{
	srand(time(NULL));
	for(int i = 0; i < size; i++) numbers[i] = rand() % 101;
}

void printArray(int *numbers, int size)
{
	for(int i = 0; i < size; i++)
		printf("numbers[%d] = %d\n", i, numbers[i]);
	printf("\n");
}

void swapArray(int *numbers, int size)
{
	for(int i = 0; i < (size / 2); i++)
	{
		int temp = numbers[size - i - 1];
		numbers[size - i - 1] = numbers[i];
		numbers[i] = temp;
	}
}
