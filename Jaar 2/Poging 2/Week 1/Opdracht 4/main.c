#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void initTime();
void generateLetters(char *data, int size);
void printArray(char *data, int size);
void sortAlpha(char *data, int size);
void swap(char *numb1, char *numb2);

int sortCnt = 0;
int swapCnt = 0;

int main()
{
	char data[10];
	int size = sizeof(data)/sizeof(char);
	initTime();
	generateLetters(data, size);
	printArray(data, size);
	sortAlpha(data, size);
	printArray(data, size);
	return 0;
}

void initTime()
{
	srand(time(NULL));
}

void generateLetters(char *data, int size)
{
	for(int i = 0; i < size; i++)
	{
		data[i] = rand() % 26 + 97;
	}
}

void printArray(char *data, int size)
{
	for(int i = 0; i < size; i++)
	{
		printf("data[%d] = %c, int: %d\n", i, data[i], data[i]);
	}
	printf("\n");
}

void sortAlpha(char *data, int size)
{
	sortCnt++;
	printf("Sorting #%d\n", sortCnt);
	for(int i = 0; i < size - 1; i++)
	{
		if(data[i] > data[i + 1])
		{
			swap(&data[i], &data[i + 1]);
			sortAlpha(data, size);
		}
	}
}

void swap(char *numb1, char *numb2)
{
	swapCnt++;
	printf("Swapping #%d\n", swapCnt);
	char temp = *numb1;
	*numb1 = *numb2;
	*numb2 = temp;
}
