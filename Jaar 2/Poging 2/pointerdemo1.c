#include <stdio.h>



/*********************************************************
    void printbyte() 
	
	print een byte, en indien van toepassing ook als
	karakter.
	 
	LET OP: het gedrag van printf bij format specifier
	%02x is verschillend bij unsigned char en bij char.
	Probeer het maar eens uit, wanneer de char een waarde
	heeft groter dan 0x80.
	
	De "normaal printbare karakters" in ascii zijn van 
	0x20 (de spatie) tot (maar niet tot en met) 0x80
*********************************************************/
void printbyte(unsigned char c ) {	
	if ((c >= 0x20) && ( c < 0x80)) {
		printf( "%02x:%c", c, c);
	} else {
		printf( "%02x:?", c);
	}	
}


/*********************************************************
   void dumpstack() 
   dump aantal bytes van de stack, startend vanaf startaddr
   oplopende adressen.
*********************************************************/
void dumpstack( const unsigned char * const startaddr, int aantal) {
	const unsigned char *  ptr = startaddr;
	/* "const" zorgt er hier voor dat via ptr er geen wijzigingen mogen
	   worden doorgevoerd op de stack */
	int i;
	for (i = 1; i<= aantal; i++) {
		printf( "%p: ", ptr );
		printbyte(*ptr);
		printf("\n");
		ptr++;
	}
}

/*********************************************************
    int main()
*********************************************************/
int main() {
    int X = 10;
	int Y = 20;
    int * iptr = &X;

    printf("Adres van X = %p\n", &X);
    printf("Adres van Y = %p\n", &Y);
    printf("Adres van iptr = %p\n", &iptr);
   
    printf("\nX = %d\n", X);
    dumpstack( (unsigned char *) &X, 4);

    printf("\nY = %d\n", Y);
    dumpstack( (unsigned char *) &Y, 4);

	printf("\niptr wijst naar X, *iptr = %d, iptr bevat de 4 bytes van adres van X:\n", *iptr);
    dumpstack( (unsigned char *) &iptr, 4);

	iptr--;
	printf("\nNa iptr-- is *iptr = %d, iptr bevat adres van Y (4 bytes):\n", *iptr);
    dumpstack( (unsigned char *) &iptr, 4);

	printf("\n\nNb op mijn virtuele machine (debian) is de groeirichting van de stack\n");
	printf("in de richting van lagere adressen. Opslag is little endian (lsb eerst).\n");
	return 0;

}
