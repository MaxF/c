#include <stdio.h>

typedef enum { false = 0, true = 1 } bool;

#define MAX_STR_LEN 100

int stringLength(char *data);
bool isPalindrome(char *data, int size);
void removeEnterKey(char *data, int size);

int main()
{
	char data[MAX_STR_LEN];
	printf("Geef een string (max %d karakters): ", MAX_STR_LEN);
	fgets(data, MAX_STR_LEN, stdin);
	int size = stringLength(data);
	if(isPalindrome(data, size))
		printf("%s is a palindrome\n", data);
	else
		printf("%s is NOT a palindrome\n", data);
	return 0;
}

int stringLength(char *data)
{
	int i;
	for(i = 0; *(data + i) != '\n'; i++) { };
	removeEnterKey(data, i);
	return i;
}

bool isPalindrome(char *data, int size)
{
	for(int i = 0; i < size / 2; i++)
	{
		if(*(data + i) != *(data + size - i - 1))
			return false;
	}
	return true;
}

void removeEnterKey(char *data, int size)
{
	*(data + size) = '\0';
}
