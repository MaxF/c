#include <stdio.h>

#define MAX_STR_LEN 100

void reverse(char *data);
void stringLength(char *data, int *res);
void swapChars(char *char1, char *char2);

int main()
{
	char a[MAX_STR_LEN];
	printf("Geef een string (max %d karakters): ", MAX_STR_LEN);
	scanf("%s", a);
	reverse(a);
	printf("Reversed output: %s\n", a);
	return 0;
}

void reverse(char *data)
{
	int length;
	stringLength(data, &length);
	for(int i = 0; i < length / 2; i++)
	{
		swapChars(&data[i], &data[length - i - 1]);
	}
}

void stringLength(char *data, int *res)
{
	int i;
	for(i = 0; data[i] != '\0'; i++);
	*res = i;
}

void swapChars(char *char1, char *char2)
{
	char temp = *char1;
	*char1 = *char2;
	*char2 = temp;
}
