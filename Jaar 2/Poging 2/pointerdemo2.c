#include <stdio.h>


void printbyte( char d ) {
	unsigned int c = d & 0xff;
	if ((c >= 0x20) && ( c < 0x80)) {
		printf( "%02x:%c", c, c);
	} else {
		printf( "%02x:?", c);
	}	
}


/*********************************************************
   void dumpstack() 
   dump aantal bytes van de stack, startend vanaf startaddr
   oplopende adressen.
*********************************************************/
void dumpstack( char * startaddr, int aantal) {
	char * ptr = startaddr;
	int byte;
	int i;
	for (i = 1; i<= aantal; i++) {
		byte = *ptr & 0xff;
		printf( "%p: ", ptr );
		printbyte(byte);
		printf("\n");
		ptr++;
	}
}



/*********************************************************
   int main()
*********************************************************/


int main() {
	char chararray[] = "adriaan";
	char *charptr = (char *) &chararray;

	printf("\nToon de stack bij charptr: adres op stack van charptr  = %p\n" , &charptr);
	printf("Op mijn virtuele machine groeit de stackpointer van hoge adressen naar lagere.\n");
	printf("Merk op dat in de groeirichting het array zelf eerst op de stack staat\n");
	printf("gevolgd door de charptr.\n\n");
	printf("Merk op dat het adres in charptr verwijst naar de a in adriaan.\n");
	printf("Merk ook op dat in de groeirichting van de stack het least significant byte\n"); 
	printf("het eerst verschijnt (little endian).\n\n");

	dumpstack( (char *) &charptr, 10);

	printf( "------------------\n" );
	printf("Toon de stack bij chararray.\n");
	printf("Adres op stack van chararray = %p\n\n" , &chararray);
	dumpstack( (char *) &chararray, 10 );
	return 0;
}
